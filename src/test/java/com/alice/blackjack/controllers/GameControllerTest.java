package com.alice.blackjack.controllers;


import com.alice.blackjack.models.Game;
import com.alice.blackjack.models.GameService;
import com.alice.blackjack.models.GameTests;
import com.alice.blackjack.models.PlayerService;
import com.alice.blackjack.utils.enums.PlayerUpdateResponses;
import com.alice.blackjack.views.Player;
import com.alice.blackjack.views.PublicGameDetails;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(GameController.class)
@AutoConfigureMockMvc
public class GameControllerTest {

    private static final String gameId1 = "f887205a-f685-4711-a31c-92ab45f8f627";
    private static final String gameId2 = "36bae809-bc90-4aa4-91e1-6c38b98305c9";

    private static final String playerId1 = "c3429885-d93c-4e7b-b84e-79ea4d8e68ec";
    private static final String playerId2 = "20cd4cd6-1a7f-4054-b1fb-0231ee3c45b0";
    private static final String playerId3 = "7106ac85-0e54-4e00-9dc9-ecd1b578b86b";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GameService gameService;

    @MockBean
    private PlayerService playerService;

    private static final int maxPlayers = 6;
    private static final Integer deckSize = 4;

    /**
     * Test for GET /blackjack/players
     * @throws Exception
     */
    @Test
    public void testGetAllPlayers() throws Exception{
        Player mockPlayer1 = new Player(playerId1, "Mock Player 1");
        Player mockPlayer2 = new Player(playerId2, "Mock Player 2");

        String expected = "[{\"id\":\"c3429885-d93c-4e7b-b84e-79ea4d8e68ec\",\"name\":\"Mock Player 1\",\"status\":\"OPEN\",\"hands\":[]}," +
                "{\"id\":\"20cd4cd6-1a7f-4054-b1fb-0231ee3c45b0\",\"name\":\"Mock Player 2\",\"status\":\"OPEN\",\"hands\":[]}]";

        given(this.playerService.getAll())
                .willReturn(Arrays.asList(mockPlayer1, mockPlayer2));

        this.mockMvc.perform(get("/blackjack/players").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().string(expected));
    }

    /**
     * Test for POST /blackjack/players
     * @throws Exception
     */
    @Test
    public void testCreatePlayer() throws Exception{
        String jsonString = "{\"name\":\"Mock Player 1\"}";
        String name = "Mock Player 1";
        Player mockPlayer1 = new Player(playerId1, "Mock Player 1");

        String expected = "{\"id\":\"c3429885-d93c-4e7b-b84e-79ea4d8e68ec\",\"name\":\"Mock Player 1\",\"status\":\"OPEN\",\"hands\":[]}";

        given(this.playerService.createPlayer(name))
                .willReturn(mockPlayer1);

        this.mockMvc.perform(post("/blackjack/players")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonString))
                .andExpect(status().isOk())
                .andExpect(content().string(expected));
    }

    /**
     * Test for GET /blackjack/players/{playerId}
     * @throws Exception
     */
    @Test
    public void testGetPlayerById() throws Exception{
        Player mockPlayer1 = new Player(playerId1, "Mock Player 1");

        String expected = "{\"id\":\"c3429885-d93c-4e7b-b84e-79ea4d8e68ec\",\"name\":\"Mock Player 1\",\"status\":\"OPEN\",\"hands\":[]}";

        given(this.playerService.getPlayer(playerId1))
                .willReturn(mockPlayer1);

        //Test for 200 ok
        this.mockMvc.perform(get("/blackjack/players/"+ playerId1).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().string(expected));


        //Test for 400 error
        expected = "Invalid playerId";

        this.mockMvc.perform(get("/blackjack/players/"+ playerId2).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andExpect(status().reason(expected));
    }

    /**
     * Test for DELETE /blackjack/players/{playerId}
     * @throws Exception
     */
    @Test
    public void testDeletePlayer() throws Exception{
        Player mockPlayer1 = new Player(playerId1, "Mock Player 1");

        String expected = "{\"id\":\"c3429885-d93c-4e7b-b84e-79ea4d8e68ec\",\"name\":\"Mock Player 1\",\"status\":\"OPEN\",\"hands\":[]}";

        given(this.playerService.removePlayer(playerId1))
                .willReturn(mockPlayer1);

        //Test for 200 ok
        this.mockMvc.perform(delete("/blackjack/players/"+ playerId1).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().string(expected));

        //Test for 400 error
        expected = "Invalid playerId";

        this.mockMvc.perform(delete("/blackjack/players/"+ playerId2).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andExpect(status().reason(expected));

    }


    /**
     * This method return json representation of game's public details
     * @param game
     * @return
     */
    private String getJsonForGame(Game game){
        PublicGameDetails gameDetails = game.getDetails();


        return "{\"id\":\"" + gameDetails.getId() + "\"," +
                "\"maxPlayers\":" + gameDetails.getMaxPlayers() + "," +
                "\"status\":\"" + gameDetails.getStatus() + "\"," +
                "\"dealer\":{\"id\":\"" + gameDetails.getDealer().getId() + "\"," +
                "\"name\":\"Dealer\",\"status\":\"" + gameDetails.getDealer().getStatus() + "\"," +
                "\"hands\":[]},\"players\":[],\"nextTurn\":null,\"nextHand\":null,\"availableMoves\":null}";

    }

    /**
     * Test for GET /blackjack/games
     * @throws Exception
     */
    @Test
    public void testGetAllGames() throws Exception{
        Game mockGame1 = new Game(gameId1, 3, 2);
        Game mockGame2 = new Game(gameId2, 6, 4);

        String expected = "[" + getJsonForGame(mockGame1) + "," + getJsonForGame(mockGame2) + "]";
        given(this.gameService.getAll())
                .willReturn(Arrays.asList(mockGame1, mockGame2));
        this.mockMvc.perform(get("/blackjack/games").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().string(expected));
    }

    /**
     * Test for POST /blackjack/games
     * @throws Exception
     */
    @Test
    public void testCreateGame() throws Exception{
        String jsonString = "{\"maxPlayers\":"+ maxPlayers + ",\"deckSize\":"+ deckSize+"}";
        Game mockGame1 = new Game(gameId1, maxPlayers, deckSize);

        String expected = getJsonForGame(mockGame1);

        given(this.gameService.createGame(maxPlayers, deckSize))
                .willReturn(mockGame1);

        this.mockMvc.perform(post("/blackjack/games")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonString))
                .andExpect(status().isOk())
                .andExpect(content().string(expected));
    }

    /**
     * Test for GET /blackjack/games/{gameId}
     * @throws Exception
     */
    @Test
    public void testGetGameById() throws Exception{
        Game mockGame1 = new Game(gameId1, maxPlayers, deckSize);

        given(this.gameService.getGame(gameId1))
                .willReturn(mockGame1);

        //Test for 200 ok
        String expected = getJsonForGame(mockGame1);

        this.mockMvc.perform(get("/blackjack/games/"+ gameId1).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().string(expected));

        //Test for 400 error
        expected = "Invalid gameId";

        this.mockMvc.perform(get("/blackjack/games/"+ gameId2).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andExpect(status().reason(expected));
    }

    /**
     * Test for DELETE /blackjack/games/{gameId}
     * @throws Exception
     */
    @Test
    public void testDeleteGame() throws Exception{
        Game mockGame1 = new Game(gameId1, maxPlayers, deckSize);

        given(this.gameService.removeGame(gameId1))
                .willReturn(mockGame1);

        //Test for 200 ok
        String expected = getJsonForGame(mockGame1);

        this.mockMvc.perform(delete("/blackjack/games/"+ gameId1).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().string(expected));

        //Test for 400 error
        expected = "Invalid gameId";

        this.mockMvc.perform(delete("/blackjack/games/"+ gameId2).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andExpect(status().reason(expected));
    }

    /**
     * Test for GET /blackjack/games/{gameId}/players
     * @throws Exception
     */
    @Test
    public void testGetGamePlayers() throws Exception{
        Game mockGame1 = new Game(gameId1, maxPlayers, deckSize);
        Player mockPlayer1 = new Player(playerId1, "Mock Player 1");

        //Add player to game
        mockGame1.addPlayer(mockPlayer1);

        given(this.gameService.getGame(gameId1))
                .willReturn(mockGame1);

        //Test for 200 ok
        String expected = "[{\"id\":\"" + playerId1 + "\",\"name\":\"Mock Player 1\",\"status\":\"WAIT\",\"hands\":[],\"winnings\":100}]";

        this.mockMvc.perform(get("/blackjack/games/" + gameId1 + "/players").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().string(expected));

        //Test for 400 error
        expected = "Invalid gameId";

        this.mockMvc.perform(get("/blackjack/games/" + gameId2 + "/players").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andExpect(status().reason(expected));
    }

    /**
     * Test for POST /blackjack/games/{gameId}/players
     * @throws Exception
     */
    @Test
    public void testAddGamePlayers() throws Exception{
        Game mockGame1 = new Game(gameId1, maxPlayers, deckSize);
        Player mockPlayer1 = new Player(playerId1, "Mock Player 1");

        given(this.gameService.getGame(gameId1))
                .willReturn(mockGame1);
        given(this.playerService.getPlayer(playerId1))
                .willReturn(mockPlayer1);

        String jsonString = "{\"id\":\"" + playerId1 + "\"}";

        //Test for 200 ok
        String expected = "\"WAIT\"";

        this.mockMvc.perform(post("/blackjack/games/" + gameId1 + "/players")
                .contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isOk()).andExpect(content().string(expected));


        //Test for 400 error
        expected = "Invalid gameId";

        this.mockMvc.perform(post("/blackjack/games/" + gameId2 + "/players")
                .contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isBadRequest()).andExpect(status().reason(expected));


        //Test for 400 error
        expected = "Invalid playerId";
        jsonString = "{\"id\":\"" + playerId2 + "\"}";
        this.mockMvc.perform(post("/blackjack/games/" + gameId1 + "/players")
                .contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isBadRequest()).andExpect(status().reason(expected));
    }

    /**
     * Test for DELETE /blackjack/games/{gameId}/players
     * @throws Exception
     */
    @Test
    public void testDeleteGamePlayers() throws Exception{
        Game mockGame1 = new Game(gameId1, maxPlayers, deckSize);
        Player mockPlayer1 = new Player(playerId1, "Mock Player 1");

        //Add player to game
        mockGame1.addPlayer(mockPlayer1);

        given(this.gameService.getGame(gameId1))
                .willReturn(mockGame1);
        given(this.playerService.getPlayer(playerId1))
                .willReturn(mockPlayer1);

        String jsonString = "{\"id\":\"" + playerId1 + "\"}";

        //Test for 200 ok
        String expected = "100";

        this.mockMvc.perform(delete("/blackjack/games/" + gameId1 + "/players")
                .contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isOk()).andExpect(content().string(expected));


        //Test for 400 error
        expected = "Invalid gameId";

        this.mockMvc.perform(delete("/blackjack/games/" + gameId2 + "/players")
                .contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isBadRequest()).andExpect(status().reason(expected));


        //Test for 400 error
        expected = "Invalid playerId";
        jsonString = "{\"id\":\"" + playerId2 + "\"}";
        this.mockMvc.perform(delete("/blackjack/games/" + gameId1 + "/players")
                .contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isBadRequest()).andExpect(status().reason(expected));
    }

    /**
     * Test for PUT /blackjack/games/{gameId}/start
     * @throws Exception
     */
    @Test
    public void testStartGame() throws Exception{
        String cardString = "ACEofSPADES, JACKofSPADES, SIXofCLUBS, SEVENofHEARTS";
        Game mockGame1 = new Game(gameId1, maxPlayers, GameTests.getCardsFromString(cardString));
        Player mockPlayer1 = new Player(playerId1, "Mock Player 1");

        //Add player to game
        mockGame1.addPlayer(mockPlayer1);

        //Get details
        PublicGameDetails details = mockGame1.getDetails();

        given(this.gameService.getGame(gameId1))
                .willReturn(mockGame1);

        //Test for 200 ok
        String expected = "{\"id\":\"f887205a-f685-4711-a31c-92ab45f8f627\",\"maxPlayers\":6,\"status\":\"STARTED\"," +
                "\"dealer\":{\"id\":\"" + details.getDealer().getId() + "\",\"name\":\"Dealer\",\"status\":\"WAIT\"," +
                "\"hands\":[{\"cards\":[{\"rank\":\"JACK\",\"suit\":\"SPADES\"},{\"rank\":\"SEVEN\",\"suit\":\"HEARTS\"}]," +
                "\"value\":17}]},\"players\":[{\"id\":\"c3429885-d93c-4e7b-b84e-79ea4d8e68ec\",\"name\":\"Mock Player 1\"," +
                "\"status\":\"TURN\",\"hands\":[{\"cards\":[{\"rank\":\"ACE\",\"suit\":\"SPADES\"},{\"rank\":\"SIX\",\"suit\":\"CLUBS\"}]," +
                "\"value\":17}],\"winnings\":90}],\"nextTurn\":{\"id\":\"c3429885-d93c-4e7b-b84e-79ea4d8e68ec\"," +
                "\"name\":\"Mock Player 1\",\"status\":\"TURN\",\"hands\":[{\"cards\":[{\"rank\":\"ACE\",\"suit\":\"SPADES\"}," +
                "{\"rank\":\"SIX\",\"suit\":\"CLUBS\"}],\"value\":17}]},\"nextHand\":{\"cards\":[{\"rank\":\"ACE\",\"suit\":\"SPADES\"}," +
                "{\"rank\":\"SIX\",\"suit\":\"CLUBS\"}],\"value\":17},\"availableMoves\":[\"STAND\",\"HIT\",\"SURRENDER\"]}";

        this.mockMvc.perform(put("/blackjack/games/" + gameId1 + "/start").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().string(expected));

        //Test for 400 error
        expected = "Invalid gameId";

        this.mockMvc.perform(put("/blackjack/games/" + gameId2 + "/start").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andExpect(status().reason(expected));
    }

    /**
     * Test for POST /blackjack/games/{gameId}/players/{playerId}
     * @throws Exception
     */
    @Test
    public void testUpdateGamePlayerMove() throws Exception{
        Game mockGame1 = new Game(gameId1, maxPlayers, deckSize);
        Player mockPlayer1 = new Player(playerId1, "Mock Player 1");

        //Add player to game
        mockGame1.addPlayer(mockPlayer1);

        given(this.gameService.getGame(gameId1))
                .willReturn(mockGame1);
        given(this.playerService.getPlayer(playerId1))
                .willReturn(mockPlayer1);

        String jsonString = "{\"move\":\"STAND\"}";

        //Test for 200 ok
        String expected = "\""+ PlayerUpdateResponses.GAME_NOT_STARTED +"\"";

        this.mockMvc.perform(post("/blackjack/games/" + gameId1 + "/players/" + playerId1)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isOk()).andExpect(content().string(expected));


        //Test for 400 error
        expected = "Invalid gameId";

        this.mockMvc.perform(post("/blackjack/games/" + gameId2 + "/players/" + playerId1)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isBadRequest()).andExpect(status().reason(expected));


        //Test for 400 error
        expected = "Invalid playerId";
        this.mockMvc.perform(post("/blackjack/games/" + gameId1 + "/players/" + playerId2)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isBadRequest()).andExpect(status().reason(expected));
    }

}
