package com.alice.blackjack.views;

import com.alice.blackjack.utils.enums.Ranks;
import com.alice.blackjack.utils.enums.Suits;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Tests all the functionality of Hand class
 */
public class HandTests {

    private static final Hand hand = new Hand();

    private static final Card aceOfSpades = new Card(Ranks.ACE, Suits.SPADES);
    private static final Card jackOfDiamonds = new Card(Ranks.JACK, Suits.DIAMONDS);
    private static final Card sixOfClubs = new Card(Ranks.SIX, Suits.CLUBS);
    private static final Card kingOfHearts = new Card(Ranks.KING, Suits.HEARTS);

    @Before
    public void clearCards(){
        hand.getCards().clear();
    }

    @Test(expected = NullPointerException.class)
    public void testAddCardException(){
        hand.addCard(null);
    }

    @Test
    public void testAddCard(){
        hand.addCard(aceOfSpades);
        hand.addCard(jackOfDiamonds);

        assertEquals( "Card{rank=ACE, suit=SPADES}",hand.getCards().getFirst().toString());
        assertEquals( "Card{rank=JACK, suit=DIAMONDS}",hand.getCards().getLast().toString());
    }

    @Test
    public void testAddCardIsCreatingImmutableCopy(){
        Hand hand = new Hand();
        Card original = new Card(Ranks.ACE, Suits.SPADES);
        // Add original card to Hand
        hand.addCard(original);

        //This should't affect existing card in Hand
        original = new Card(Ranks.JACK, Suits.DIAMONDS);

        assertEquals( "Card{rank=ACE, suit=SPADES}",hand.getCards().getFirst().toString());
    }

    /**
     * This test checks if getValue() can correctly value both Soft and Hard Hand
     */
    @Test
    public void testGetValue(){
        Hand hand = new Hand();

        // Test for Soft Hand where Ace is valued as 11
        hand.addCard(aceOfSpades);
        hand.addCard(sixOfClubs);
        assertEquals("Soft Hand getValue", 17, hand.getValue());

        // Test for Hard Hand where Ace is valued as 1
        hand.addCard(aceOfSpades);
        assertEquals("Hard Hand getValue", 18, hand.getValue());

        // Test to check getValue method convert Aces to 1 that were previously valued at 11
        hand.addCard(sixOfClubs);
        assertEquals("Hard Hand getValue", 14, hand.getValue());
    }

    /**
     * This tests if canSplit() returns correct answer if same value split is allowed or not allowed
     */
    @Test
    public void testCanSplit() {
        // Testing for same value split allowed
        hand.addCard(jackOfDiamonds);
        hand.addCard(kingOfHearts);

        assertTrue("Same value split allowed positive test", hand.canSplit(true));

        clearCards();
        hand.addCard(jackOfDiamonds);
        hand.addCard(sixOfClubs);

        assertFalse("Same value split allowed negative test", hand.canSplit(true));

        // Test to check only that split is allowed only for two cards in Hand
        hand.addCard(jackOfDiamonds);
        hand.addCard(sixOfClubs);

        assertFalse("Same value split allowed negative test", hand.canSplit(true));


        // testing same value split not allowed
        clearCards();
        hand.addCard(jackOfDiamonds);
        hand.addCard(new Card(Ranks.JACK, Suits.CLUBS));
        assertTrue("Same value split not allowed positive test", hand.canSplit(false));

        clearCards();
        hand.addCard(jackOfDiamonds);
        hand.addCard(kingOfHearts);
        assertFalse("Same value split not allowed negative test", hand.canSplit(false));
    }

    @Test(expected = IllegalStateException.class)
    public void testSplitException() {
        hand.addCard(aceOfSpades);
        hand.addCard(jackOfDiamonds);

        //Same value split is true or false, this should always throw IllegalStateException
        hand.split(true);
    }

    @Test
    public void testSplit(){
        // Testing for same value split allowed
        hand.addCard(jackOfDiamonds);
        hand.addCard(kingOfHearts);

        Hand splitHand = hand.split(true);
        assertEquals(1, hand.getCards().size());
        assertEquals(1, splitHand.getCards().size());
        assertEquals("Card{rank=JACK, suit=DIAMONDS}", hand.getCards().getFirst().toString());
        assertEquals("Card{rank=KING, suit=HEARTS}", splitHand.getCards().getFirst().toString());

        clearCards();

        // Testing for same value split not allowed
        hand.addCard(jackOfDiamonds);
        hand.addCard(new Card(Ranks.JACK, Suits.CLUBS));

        splitHand = hand.split(false);
        assertEquals(1, hand.getCards().size());
        assertEquals(1, splitHand.getCards().size());
        assertEquals("Card{rank=JACK, suit=DIAMONDS}", hand.getCards().getFirst().toString());
        assertEquals("Card{rank=JACK, suit=CLUBS}", splitHand.getCards().getFirst().toString());
    }

    @Test
    public void getCards() {
        hand.addCard(jackOfDiamonds);

        assertEquals(1, hand.getCards().size());
        assertEquals("Card{rank=JACK, suit=DIAMONDS}", hand.getCards().getFirst().toString());
    }

    @Test
    public void testToString() throws Exception {
        hand.addCard(jackOfDiamonds);
        hand.addCard(aceOfSpades);

        assertEquals("Hand{cards=[Card{rank=JACK, suit=DIAMONDS}, Card{rank=ACE, suit=SPADES}]}", hand.toString());
    }

}