package com.alice.blackjack.views;

import com.alice.blackjack.utils.enums.GameStatus;
import com.alice.blackjack.utils.enums.Moves;
import com.alice.blackjack.utils.enums.Ranks;
import com.alice.blackjack.utils.enums.Suits;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class PublicGameDetailsTests {
    private static final String gameId = "4f1ade5e-e726-4562-9b48-84def40b00da";
    private static final int maxPlayers = 6;
    private static final GameStatus status = GameStatus.STARTED;
    private static final String dealerId = "3a8814c2-20ad-4a7e-95e7-3b4bb5a7f325";
    private static final String playerId = "c3429885-d93c-4e7b-b84e-79ea4d8e68ec";
    private static final int playerWinnings = 100;

    private static final Card jackOfDiamonds = new Card(Ranks.JACK, Suits.DIAMONDS);
    private static final Card sixOfClubs = new Card(Ranks.SIX, Suits.CLUBS);
    private static final Card kingOfHearts = new Card(Ranks.KING, Suits.HEARTS);

    private static Player dealer;
    private static Player player;
    private static List<Player> players;
    private static List<PublicGameDetails.PublicPlayer> publicPlayers;
    private static Map<String, Integer> winningsMap;

    private static Player nextTurn;
    private static Hand nextHand;
    private static Set<Moves> availableMoves;
    private static PublicGameDetails gameDetails;

    @BeforeClass
    public static void setUPGameDetails(){
        //Set up dealer
        dealer = new Player(dealerId, "Dealer");
        Hand dealerHand = new Hand();
        dealerHand.addCard(kingOfHearts);
        dealerHand.addCard(jackOfDiamonds);
        dealer.getHands().add(dealerHand);

        //Set up player
        player = new Player(playerId, "Player");
        Hand playerHand = new Hand();
        playerHand.addCard(jackOfDiamonds);
        playerHand.addCard(sixOfClubs);
        player.getHands().add(playerHand);

        players = new ArrayList<>();
        players.add(player);

        winningsMap = new HashMap<>();
        winningsMap.put(player.getId(), playerWinnings);

        nextHand = playerHand;
        nextTurn = player;
        availableMoves = new LinkedHashSet<>(Arrays.asList(Moves.STAND, Moves.HIT, Moves.SURRENDER));
        gameDetails = new PublicGameDetails(gameId, maxPlayers, status, dealer, players, winningsMap, nextHand, nextTurn, availableMoves);
    }


    @Test
    public void testGetters(){
        assertSame(gameId, gameDetails.getId());
        assertSame(maxPlayers, gameDetails.getMaxPlayers());
        assertSame(status, gameDetails.getStatus());
        assertSame(dealer, gameDetails.getDealer());
        assertSame(nextTurn, gameDetails.getNextTurn());
        assertSame(nextHand, gameDetails.getNextHand());
        assertSame(availableMoves, gameDetails.getAvailableMoves());
        //PublicPlayer is a nested class so can't create its object. Check using toString method
        assertEquals("[Player{id='"+ playerId + "', name='Player', hands=" + player.getHands() + "', winnings=" + playerWinnings +"}]",
                gameDetails.getPlayers().toString());
    }

    @Test
    public void testToString() throws Exception {
        assertEquals("PublicGameDetails{id='"+ gameId + "', maxPlayers=" + maxPlayers + ", status=" + status + ", dealer=" + dealer +", " +
                "players=[Player{id='"+ playerId + "', name='Player', hands=" + player.getHands() + "', winnings=" + playerWinnings +"}], " +
                "nextTurn=" + nextTurn + ", nextHand=" + nextHand + ", availableMoves="+ availableMoves + "}", gameDetails.toString());
    }

}