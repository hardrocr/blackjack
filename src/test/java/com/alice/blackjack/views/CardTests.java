package com.alice.blackjack.views;

import com.alice.blackjack.utils.enums.Ranks;
import com.alice.blackjack.utils.enums.Suits;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * This class tests all the functionality of Card class
 */
public class CardTests {
    @Test(expected = NullPointerException.class)
    public void testAllNull(){
        new Card(null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testNullRank(){
        new Card(null, Suits.CLUBS);
    }

    @Test(expected = NullPointerException.class)
    public void testNullsuit(){
        new Card(Ranks.ACE, null);
    }

    @Test
    public void testGetRank(){
        Card card = new Card(Ranks.ACE, Suits.SPADES);
        assertEquals("Testing getRank", Ranks.ACE, card.getRank());
    }

    @Test
    public void testGetSuit(){
        Card card = new Card(Ranks.ACE, Suits.SPADES);
        assertEquals("Testing getSuit", Suits.SPADES, card.getSuit());
    }

    @Test
    public void testToString() {
        assertEquals("testing toString()", "Card{rank=JACK, suit=CLUBS}", new Card(Ranks.JACK, Suits.CLUBS).toString());
    }
}
