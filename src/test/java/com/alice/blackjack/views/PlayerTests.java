package com.alice.blackjack.views;

import com.alice.blackjack.utils.enums.PlayerStatus;
import com.alice.blackjack.utils.enums.Ranks;
import com.alice.blackjack.utils.enums.Suits;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class PlayerTests {
    private static final String PLAYERID = "c20d0989-d378-4cb0-b676-1d779d802d66";
    private static final String PLAYERNAME = "Test Player";

    private void addHandwithAceOfSpades(Player player){
        Hand hand = new Hand();
        hand.addCard(new Card(Ranks.ACE, Suits.SPADES));
        player.getHands().add(hand);
    }
            
    @Test
    public void testGetters() throws Exception {
        Player player = new Player(PLAYERID, PLAYERNAME);
        assertEquals(PLAYERID, player.getId());
        assertEquals(PLAYERNAME, player.getName());
        assertSame(PlayerStatus.OPEN, player.getStatus());

        addHandwithAceOfSpades(player);
        assertEquals(1, player.getHands().size());
        assertEquals("[Hand{cards=[Card{rank=ACE, suit=SPADES}]}]", player.getHands().toString());
    }

    @Test
    public void testResetHands() throws Exception {
        Player player = new Player(PLAYERID, PLAYERNAME);
        addHandwithAceOfSpades(player);
        assertEquals(1, player.getHands().size());
        
        player.resetHands();
        assertEquals(0, player.getHands().size());
        
    }

    @Test
    public void testSetName() throws Exception {
        Player player = new Player(PLAYERID, PLAYERNAME);
        player.setName("Test Player 2");
        assertEquals("Test Player 2", player.getName());
    }

    @Test
    public void testSetStatus() throws Exception {
        Player player = new Player(PLAYERID, PLAYERNAME);
        player.setStatus(PlayerStatus.WON);
        assertSame(PlayerStatus.WON, player.getStatus());
    }

    @Test
    public void testToString() throws Exception {
        Player player = new Player(PLAYERID, PLAYERNAME);
        addHandwithAceOfSpades(player);

        assertEquals(
                "Player{id='" + PLAYERID + "', name='" + PLAYERNAME + "', hands=[Hand{cards=[Card{rank=ACE, suit=SPADES}]}]}"
                , player.toString());
    }

}