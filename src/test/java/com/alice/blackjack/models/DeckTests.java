package com.alice.blackjack.models;

import com.alice.blackjack.utils.enums.Ranks;
import com.alice.blackjack.utils.enums.Suits;
import com.alice.blackjack.views.Card;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

import static org.junit.Assert.*;

public class DeckTests {

    private static final Card aceOfSpades = new Card(Ranks.ACE, Suits.SPADES);
    private static final Card jackOfDiamonds = new Card(Ranks.JACK, Suits.DIAMONDS);
    private static final Card sixOfClubs = new Card(Ranks.SIX, Suits.CLUBS);
    private static final Card kingOfHearts = new Card(Ranks.KING, Suits.HEARTS);

    @Test
    public void testDeckHaveCorrectCards() {
        //Picks a random deck size from 1 - 10
        int deckSize = new Random(10).nextInt() + 1;
        Deck deck = new Deck(deckSize);

        for(int i = 0 ; i < deckSize; i++){
            for(Suits suit : Suits.values()){
                for(Ranks rank : Ranks.values()){
                    Card card = deck.draw();
                    //Check Rank and Suit match
                    assertEquals(suit, card.getSuit());
                    assertEquals(rank, card.getRank());
                }
            }
        }
    }

    @Test
    public void testEmptyDeckException(){
        Deck deck = new Deck(1);

        //Draw 52 cards
        for(int i = 0; i < 52; i ++){
            deck.draw();
        }

        try{
            //this should throw NoSuchElementException
            deck.draw();
        } catch (NoSuchElementException ex){
            assertEquals("No more cards left in Deck", ex.getMessage());
        }
    }

    @Test
    public void testDeckCreatesImmutableList(){
        List<Card> cards = new ArrayList<>();
        cards.add(aceOfSpades);
        cards.add(jackOfDiamonds);
        cards.add(sixOfClubs);

        Deck deck = new Deck(cards);

        //Making any changes to the list passed to the constructor shouldn't affect the deck
        cards.set(0, null);
        cards.set(1, kingOfHearts);
        cards.remove(2);

        assertSame(aceOfSpades, deck.draw());
        assertSame(jackOfDiamonds, deck.draw());
        assertSame(sixOfClubs, deck.draw());


        try{
            //this should throw NoSuchElementException
            deck.draw();
        } catch (NoSuchElementException ex){
            assertEquals("No more cards left in Deck", ex.getMessage());
        }
    }

    @Test
    public void testShuffle() throws Exception {
        Deck deck = new Deck(1);

        //test to check findFirstShuffledCardIndex method is working
        assertEquals(52, findFirstShuffledCardIndex(deck));

        // Because deck is empty, so recreate deck
        deck = new Deck(1);
        deck.shuffle();

        assertTrue(findFirstShuffledCardIndex(deck) < 52);
    }

    /**
     * This method returns index of first card from deck that appears shuffled and not in its correct position according
     * to ordered deck. If all cards are in order this method returns a number = deckSize * 52.
     *
     * This method draws one card from deck. if card is not in order returns index of drawn card otherwise draws next card
     * If all cards are in order deck will be empty.
     *
     * @param deck Deck from which first shuffled card need to be identified
     * @return index of first card that appears to be shuffled, if all cards are in order return deckSize * 52
     */
    private int findFirstShuffledCardIndex(Deck deck){
        int i = 0;
        for(Suits suit : Suits.values()){
            for(Ranks rank : Ranks.values()){
                Card card = deck.draw();

                //If either card or Suit is different
                if(!card.getRank().equals(rank) || !card.getSuit().equals(suit)){
                    return i;
                }
                i++;
            }
        }

        return i;
    }

}