package com.alice.blackjack.models;

import com.alice.blackjack.views.Player;
import org.junit.Test;

import static org.junit.Assert.*;

public class PlayerServiceTests {

    @Test
    public void testCreatePlayer() {
        PlayerService playerService = new PlayerService();
        Player player = playerService.createPlayer("Player 1");

        //Check player object is created
        assertNotNull(player);

        //Check Player has correct name
        assertEquals("Player 1", player.getName());

        //Check player id is UUID
        assertTrue(player.getId().matches("[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"));

        //Check player's hands ae empty
        assertEquals(0, player.getHands().size());
    }

    @Test
    public void testGetAll() {
        PlayerService playerService = new PlayerService();

        assertEquals(0, playerService.getAll().size());

        playerService.createPlayer("Player 1");
        playerService.createPlayer("Player 2");

        assertEquals(2, playerService.getAll().size());
    }

    @Test
    public void testGetPlayer() {
        PlayerService playerService = new PlayerService();

        Player player1 = playerService.createPlayer("Player 1");
        Player player2 = playerService.createPlayer("Player 2");

        //Test to check correct Player is returned by getPlayer method
        assertSame(player1, playerService.getPlayer(player1.getId()));
        assertSame(player2, playerService.getPlayer(player2.getId()));

        //Test for player id that doesn't exists
        assertNull(playerService.getPlayer("this is not a player id"));
    }

    @Test
    public void testRemovePlayer() {
        PlayerService playerService = new PlayerService();
        Player player = playerService.createPlayer("Player 1");

        //Test for player id that doesn't exists
        playerService.removePlayer("this is not a player id");
        assertEquals(1, playerService.getAll().size());

        //Test for player id that exists
        playerService.removePlayer(player.getId());
        assertEquals(0, playerService.getAll().size());
    }

}