package com.alice.blackjack.models;

import com.alice.blackjack.utils.enums.Moves;
import com.alice.blackjack.utils.enums.Ranks;
import com.alice.blackjack.utils.enums.Suits;
import com.alice.blackjack.views.Card;
import com.alice.blackjack.views.Hand;
import org.junit.Test;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PlayerMoveHandlerTests {

    private static final Card aceOfSpades = new Card(Ranks.ACE, Suits.SPADES);
    private static final Card jackOfDiamonds = new Card(Ranks.JACK, Suits.DIAMONDS);
    private static final Card sixOfClubs = new Card(Ranks.SIX, Suits.CLUBS);
    private static final Card kingOfHearts = new Card(Ranks.KING, Suits.HEARTS);

    private static Set<Moves> allMoves;

    static {
        allMoves = new LinkedHashSet<>();
        allMoves.add(Moves.STAND);
        allMoves.add(Moves.HIT);
        allMoves.add(Moves.SPLIT);
        allMoves.add(Moves.SURRENDER);
    }

    @Test
    public void getAvailableMoves() {
        Hand hand = new Hand();
        hand.addCard(jackOfDiamonds);
        hand.addCard(kingOfHearts);

        Set<Moves> availableMoves = PlayerMoveHandler.getAvailableMoves(hand, true);
        assertEquals(allMoves.size(), availableMoves.size());
        assertTrue(availableMoves.containsAll(allMoves));

        hand.addCard(aceOfSpades);
        //Hand is at 21
        availableMoves = PlayerMoveHandler.getAvailableMoves(hand, true);
        assertEquals(2, availableMoves.size());
        assertTrue(availableMoves.contains(Moves.STAND));
        assertTrue(availableMoves.contains(Moves.SURRENDER));

        hand.addCard(sixOfClubs);
        //Hand is bust
        availableMoves = PlayerMoveHandler.getAvailableMoves(hand, true);
        assertEquals(1, availableMoves.size());
        assertTrue(availableMoves.contains(Moves.SURRENDER));
    }

}