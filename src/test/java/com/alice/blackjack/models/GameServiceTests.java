package com.alice.blackjack.models;

import org.junit.Test;

import static org.junit.Assert.*;

public class GameServiceTests {

    @Test
    public void testCreateGame() {
        GameService gameService = new GameService();
        Integer deckSize = 2;
        int maxPlayers = 3;
        Game game = gameService.createGame(maxPlayers, deckSize);

        // Check game object is created and is not null
        assertNotNull(game);

        // Check if game has correct number of max players
        assertEquals(maxPlayers, game.getMaxPlayers());

        // Check if game has correct deck size
        assertEquals(deckSize, game.getDeckSize());

        //Check game id is UUID
        assertTrue(game.getId().matches("[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"));

    }

    @Test
    public void testGetAll() {
        GameService gameService = new GameService();

        assertEquals(0, gameService.getAll().size());

        gameService.createGame(3, 2);
        gameService.createGame(6, 4);

        assertEquals(2, gameService.getAll().size());
    }

    @Test
    public void testGetGame() {
        GameService gameService = new GameService();

        Game game1 = gameService.createGame(3, 2);
        Game game2 = gameService.createGame(6, 4);

        //Test to check correct Game is returned by getGame method
        assertSame(game1, gameService.getGame(game1.getId()));
        assertSame(game2, gameService.getGame(game2.getId()));

        //Test for game id that doesn't exists
        assertNull(gameService.getGame("this is not a game id"));
    }

    @Test
    public void testRemoveGame() {
        GameService gameService = new GameService();
        Game game = gameService.createGame(3, 2);

        //Test for game id that doesn't exists
        gameService.removeGame("this is not a game id");
        assertEquals(1, gameService.getAll().size());

        //Test for game id that exists
        gameService.removeGame(game.getId());
        assertEquals(0, gameService.getAll().size());
    }

}