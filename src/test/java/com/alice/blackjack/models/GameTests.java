package com.alice.blackjack.models;

import com.alice.blackjack.utils.enums.*;
import com.alice.blackjack.views.Card;
import com.alice.blackjack.views.Hand;
import com.alice.blackjack.views.Player;
import com.alice.blackjack.views.PublicGameDetails;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class GameTests {
    private static final String gameId1 = "f887205a-f685-4711-a31c-92ab45f8f627";
    private static final String gameId2 = "36bae809-bc90-4aa4-91e1-6c38b98305c9";

    private static final String playerId1 = "c3429885-d93c-4e7b-b84e-79ea4d8e68ec";
    private static final String playerId2 = "20cd4cd6-1a7f-4054-b1fb-0231ee3c45b0";
    private static final String playerId3 = "7106ac85-0e54-4e00-9dc9-ecd1b578b86b";
    private static final String playerName = "Test player";

    private static final int maxPlayers = 6;
    private static final Integer deckSize = 4;

    /**
     * String should be in the format of "RANKofSUIT, RANKofSUIT, RANKofSUIT"
     * eg ACEofSPADES, JACKofDIAMONDS, KINGofHEART
     * @param cardString string representation of cards
     * @return
     */
    public static List<Card> getCardsFromString(String cardString){
        List<Card> cards = new ArrayList<>();
        //Split all options by ", "
        for(String s : cardString.split(", ")){

            //Split this string using of
            String[] components = s.split("of");

            //Make sure there are two components
            if(components.length == 2){
                // get rank from first and suit from second component
                Ranks rank = Ranks.valueOf(components[0]);
                Suits suit = Suits.valueOf(components[1]);

                if(rank != null && suit != null){
                    cards.add(new Card(rank, suit));
                }
            }
        }

        return cards;
    }

    @Test
    public void testStartGame() {
        Player player = new Player(playerId1, playerName);
        String cardString = "ACEofSPADES, JACKofSPADES, SIXofCLUBS, SEVENofHEARTS";
        Game game = new Game(gameId1, maxPlayers, getCardsFromString(cardString));
        game.addPlayer(player);

        assertSame(GameStatus.OPEN, game.getStatus());
        //Check Player's hands are empty
        assertEquals(0, player.getHands().size());

        //Start game
        PublicGameDetails details = game.startGame();
        assertSame(GameStatus.STARTED, game.getStatus());

        //Check both dealer and player have one hand each with two cards
        assertEquals(1, details.getDealer().getHands().size());
        assertEquals(1, player.getHands().size());

        assertEquals(2, details.getDealer().getHands().get(0).getCards().size());
        assertEquals(2, player.getHands().get(0).getCards().size());
    }

    @Test
    public void addPlayer() {
        Player player1 = new Player(playerId1, playerName);
        Player player2 = new Player(playerId2, playerName);

        String cardString = "ACEofSPADES, JACKofSPADES, SIXofCLUBS, SEVENofHEARTS";
        Game game = new Game(gameId1, 1, getCardsFromString(cardString));
        Game game2 = new Game(gameId2, 1, getCardsFromString(cardString));

        // check player's status is open and there are no players in the game
        assertEquals(0, game.getDetails().getPlayers().size());
        assertSame(PlayerStatus.OPEN, player1.getStatus());

        //Add player to the game
        game.addPlayer(player1);

        //Check player's status change to wait and there is one player in game
        assertEquals(1, game.getDetails().getPlayers().size());
        assertSame(PlayerStatus.WAIT, player1.getStatus());

        //Test to check that player playing one game shouldn't be added to other games
        assertSame(PlayerStatus.OPEN, game2.addPlayer(player1));

        //Test to check that addPlayer method doesn't add more than max players
        assertSame(PlayerStatus.OPEN, game.addPlayer(player2));
    }

    @Test
    public void testRemovePlayer() {
        Player player1 = new Player(playerId1, playerName);
        Player player2 = new Player(playerId2, playerName);

        String cardString = "ACEofSPADES, JACKofSPADES, SIXofCLUBS, SEVENofHEARTS";
        Game game = new Game(gameId1, 1, getCardsFromString(cardString));

        // Just a quick test to check player is added
        assertSame(PlayerStatus.WAIT, game.addPlayer(player1));

        //remove player
        assertEquals(100, game.removePlayer(player1));

        // check player's status is open and there are no players in the game
        assertEquals(0, game.getDetails().getPlayers().size());
        assertSame(PlayerStatus.OPEN, player1.getStatus());

        // Test to make sure their hands are cleared.
        assertEquals(0, player1.getHands().size());

        //Check to remove player that haven't joined the game
        assertEquals(-1, game.removePlayer(player2));
    }

    @Test
    public void testGetDetails() {
        Player player = new Player(playerId1, playerName);

        String cardString = "ACEofSPADES, JACKofSPADES, SIXofCLUBS, SEVENofHEARTS, FOURofCLUBS";
        Game game = new Game(gameId1, maxPlayers, getCardsFromString(cardString));

        //Add player to game
        game.addPlayer(player);
        PublicGameDetails details = game.getDetails();

        //Check details before game started
        assertEquals(gameId1, details.getId());
        assertEquals(maxPlayers, details.getMaxPlayers());
        assertSame(GameStatus.OPEN, details.getStatus());
        assertEquals(0, details.getDealer().getHands().size());
        assertEquals(1, details.getPlayers().size());
        assertEquals(playerId1, details.getPlayers().get(0).getId());
        assertEquals(playerName, details.getPlayers().get(0).getName());
        assertSame(PlayerStatus.WAIT, details.getPlayers().get(0).getStatus());
        assertEquals(0, details.getPlayers().get(0).getHands().size());
        assertEquals(100, details.getPlayers().get(0).getWinnings());
        assertNull(details.getNextTurn());
        assertNull(details.getNextHand());
        assertNull(details.getAvailableMoves());

        //Start game
        details = game.startGame();
        //Check details after game started
        assertEquals(gameId1, details.getId());
        assertEquals(maxPlayers, details.getMaxPlayers());
        assertSame(GameStatus.STARTED, details.getStatus());
        assertEquals(1, details.getDealer().getHands().size());
        assertTrue(checkCardsAreSame(details.getDealer().getHands().get(0).getCards(), getCardsFromString("JACKofSPADES, SEVENofHEARTS")));
        assertEquals(1, details.getPlayers().size());
        assertEquals(playerId1, details.getPlayers().get(0).getId());
        assertEquals(playerName, details.getPlayers().get(0).getName());
        assertSame(PlayerStatus.TURN, details.getPlayers().get(0).getStatus());
        assertEquals(1, details.getPlayers().get(0).getHands().size());
        assertTrue(checkCardsAreSame(details.getPlayers().get(0).getHands().get(0).getCards(), getCardsFromString("ACEofSPADES, SIXofCLUBS")));
        assertEquals(90, details.getPlayers().get(0).getWinnings());
        assertSame(player, details.getNextTurn());
        Hand playerHand = player.getHands().get(0);
        assertSame(playerHand, details.getNextHand());
        Set<Moves> moves = PlayerMoveHandler.getAvailableMoves(playerHand, game.isSameValueSplitAllowed());
        assertTrue(checkMovesAreSame(moves, details.getAvailableMoves()));

        // Update game when player selects HIT
        game.updatePlayer(player, Moves.HIT);
        details = game.getDetails();

        //Check details after player selected hit
        assertEquals(gameId1, details.getId());
        assertEquals(maxPlayers, details.getMaxPlayers());
        assertSame(GameStatus.FINISHED, details.getStatus());
        assertEquals(1, details.getDealer().getHands().size());
        assertTrue(checkCardsAreSame(details.getDealer().getHands().get(0).getCards(), getCardsFromString("JACKofSPADES, SEVENofHEARTS, ACEofSPADES, JACKofSPADES")));
        assertEquals(1, details.getPlayers().size());
        assertEquals(playerId1, details.getPlayers().get(0).getId());
        assertEquals(playerName, details.getPlayers().get(0).getName());
        assertSame(PlayerStatus.WON, details.getPlayers().get(0).getStatus());
        assertEquals(1, details.getPlayers().get(0).getHands().size());
        assertTrue(checkCardsAreSame(details.getPlayers().get(0).getHands().get(0).getCards(), getCardsFromString("ACEofSPADES, SIXofCLUBS, FOURofCLUBS")));
        assertEquals(110, details.getPlayers().get(0).getWinnings());
        assertNull(details.getNextTurn());
        assertNull(details.getNextHand());
        assertNull(details.getAvailableMoves());

    }

    private boolean checkMovesAreSame(Set<Moves> set1, Set<Moves> set2){
        if(set1.size() == set2.size() && set1.containsAll(set2)){
            return true;
        } else {
            return false;
        }
    }

    private boolean checkCardsAreSame(List<Card> cards1, List<Card> cards2){
        if(cards1.toString().equals(cards2.toString())){
            return true;
        } else {
            return false;
        }
    }

    @Test
    public void updatePlayer() {
        Player player1 = new Player(playerId1, playerName);
        Player player2 = new Player(playerId2, playerName);
        Player player3 = new Player(playerId3, playerName);

        String cardString = "ACEofSPADES, JACKofSPADES, SIXofCLUBS, SEVENofHEARTS, FOURofCLUBS, ACEofDIAMONDS";
        Game game = new Game(gameId1, maxPlayers, getCardsFromString(cardString));

        //Check player can't update till game starts
        assertEquals(PlayerUpdateResponses.GAME_NOT_STARTED, game.updatePlayer(player1, Moves.HIT));

        //Add players to game
        game.addPlayer(player1);
        game.addPlayer(player2);

        //Start game
        game.startGame();

        //Check player can't Update if they have not joined the game
        assertEquals(PlayerUpdateResponses.PLAYER_NOT_IN_GAME, game.updatePlayer(player3, Moves.HIT));

        //Check player can't Update if its not their turn
        assertEquals(PlayerUpdateResponses.WAIT_FOR_TURN, game.updatePlayer(player2, Moves.HIT));

        //Check player can't Update if move is not a valid move
        assertEquals(PlayerUpdateResponses.INVALID_MOVE, game.updatePlayer(player1, Moves.SPLIT));

        //Check correct player can make correct move
        assertEquals(PlayerUpdateResponses.MOVE_RECORDED, game.updatePlayer(player1, Moves.STAND));

        // Player 2 makes a move
        assertEquals(PlayerUpdateResponses.MOVE_RECORDED, game.updatePlayer(player2, Moves.HIT));
        assertEquals(PlayerUpdateResponses.MOVE_RECORDED, game.updatePlayer(player2, Moves.STAND));

        //Game should finiah at this point
        assertSame(GameStatus.FINISHED, game.getStatus());

        //Check player can't update if game is finished
        assertEquals(PlayerUpdateResponses.GAME_FINISHED, game.updatePlayer(player1, Moves.STAND));
    }

    @Test
    public void testGetters() {
        Game game = new Game(gameId1, maxPlayers, deckSize);
        game.setPlayerBetAmount(10);
        game.setPlayerStartingAmount(100);
        game.setSameValueSplitAllowed(true);

        assertEquals(gameId1, game.getId());
        assertEquals(maxPlayers, game.getMaxPlayers());
        assertEquals(deckSize, game.getDeckSize());
        assertEquals(10, game.getPlayerBetAmount());
        assertEquals(100, game.getPlayerStartingAmount());
        assertEquals(true, game.isSameValueSplitAllowed());
        assertEquals(GameStatus.OPEN, game.getStatus());
    }


    /**
     * All the following tests are there to check game play works properly
     */

    @Test
    public void testBothPlayerAndDealerHaveBlackJack(){
        Player player = new Player(playerId1, playerName);

        String cardString = "ACEofSPADES, JACKofSPADES, KINGofCLUBS, ACEofHEARTS";
        Game game = new Game(gameId1, maxPlayers, getCardsFromString(cardString));

        //Add player to game
        game.addPlayer(player);

        game.startGame();

        //Assert Draw
        assertSame(GameStatus.FINISHED, game.getStatus());
        assertSame(PlayerStatus.DRAW, player.getStatus());
    }

    @Test
    public void testDealerHaveBlackJackAndPlayerLost(){
        Player player = new Player(playerId1, playerName);

        String cardString = "ACEofSPADES, JACKofSPADES, SIXofCLUBS, ACEofHEARTS";
        Game game = new Game(gameId1, maxPlayers, getCardsFromString(cardString));

        //Add player to game
        game.addPlayer(player);

        game.startGame();

        //Assert Player Lost
        assertSame(GameStatus.FINISHED, game.getStatus());
        assertSame(PlayerStatus.LOST, player.getStatus());
    }

    @Test
    public void testPlayerHaveBlackJackAndDealerLost(){
        Player player = new Player(playerId1, playerName);

        String cardString = "ACEofSPADES, JACKofSPADES, JACKofCLUBS, SIXofHEARTS";
        Game game = new Game(gameId1, maxPlayers, getCardsFromString(cardString));

        //Add player to game
        game.addPlayer(player);

        game.startGame();

        //Assert Player Won
        assertSame(GameStatus.FINISHED, game.getStatus());
        assertSame(PlayerStatus.WON, player.getStatus());
    }

    @Test
    public void testBothPlayerAndDealerHave21(){
        Player player = new Player(playerId1, playerName);

        String cardString = "ACEofSPADES, JACKofSPADES, SIXofCLUBS, SIXofHEARTS, FOURofCLUBS, FIVEofHEARTS";
        Game game = new Game(gameId1, maxPlayers, getCardsFromString(cardString));

        //Add player to game
        game.addPlayer(player);

        game.startGame();

        assertSame(PlayerUpdateResponses.MOVE_RECORDED, game.updatePlayer(player, Moves.HIT));

        //Assert Draw
        assertSame(GameStatus.FINISHED, game.getStatus());
        assertSame(PlayerStatus.DRAW, player.getStatus());
    }

    @Test
    public void testPlayerHave21AndDealerlost(){
        Player player = new Player(playerId1, playerName);

        String cardString = "ACEofSPADES, SEVENofSPADES, SIXofCLUBS, SIXofHEARTS, FOURofCLUBS, FIVEofHEARTS";
        Game game = new Game(gameId1, maxPlayers, getCardsFromString(cardString));

        //Add player to game
        game.addPlayer(player);

        game.startGame();

        assertSame(PlayerUpdateResponses.MOVE_RECORDED, game.updatePlayer(player, Moves.HIT));

        //Assert player won
        assertSame(GameStatus.FINISHED, game.getStatus());
        assertSame(PlayerStatus.WON, player.getStatus());
    }

    @Test
    public void testDealerHave21AndPlayerlost(){
        Player player = new Player(playerId1, playerName);

        String cardString = "ACEofSPADES, SEVENofSPADES, FIVEofCLUBS, SEVENofHEARTS, SEVENofHEARTS";
        Game game = new Game(gameId1, maxPlayers, getCardsFromString(cardString));

        //Add player to game
        game.addPlayer(player);

        game.startGame();

        assertSame(PlayerUpdateResponses.MOVE_RECORDED, game.updatePlayer(player, Moves.STAND));

        //Assert Player Lost
        assertSame(GameStatus.FINISHED, game.getStatus());
        assertSame(PlayerStatus.LOST, player.getStatus());
    }

    @Test
    public void testDealerDrawTill17(){
        Player player = new Player(playerId1, playerName);

        String cardString = "SIXofSPADES, ACEofSPADES, FIVEofCLUBS, TWOofHEARTS, TWOofCLUBS, TWOofDIAMONDS, THREEofCLUBS";
        Game game = new Game(gameId1, maxPlayers, getCardsFromString(cardString));

        //Add player to game
        game.addPlayer(player);

        game.startGame();

        assertSame(PlayerUpdateResponses.MOVE_RECORDED, game.updatePlayer(player, Moves.STAND));

        //Assert Draw
        assertSame(GameStatus.FINISHED, game.getStatus());
        assertSame(PlayerStatus.LOST, player.getStatus());
        PublicGameDetails details = game.getDetails();
        assertEquals(17, details.getDealer().getHands().get(0).getValue());
    }

}