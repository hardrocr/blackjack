package com.alice.blackjack.models;

import com.alice.blackjack.utils.enums.Ranks;
import com.alice.blackjack.utils.enums.Suits;
import com.alice.blackjack.views.Card;
import com.alice.blackjack.views.Hand;
import org.junit.Test;

import static org.junit.Assert.*;

public class EvaluateHandTests {

    private static final Card aceOfSpades = new Card(Ranks.ACE, Suits.SPADES);
    private static final Card jackOfDiamonds = new Card(Ranks.JACK, Suits.DIAMONDS);
    private static final Card sixOfClubs = new Card(Ranks.SIX, Suits.CLUBS);
    private static final Card kingOfHearts = new Card(Ranks.KING, Suits.HEARTS);

    /**
     * This test checks that isBlackjack() method returns true only if Hand's value is 21 and there are exactly 2 cards
     * in Hand
     */
    @Test
    public void testIsBlackjack() {
        Hand hand = new Hand();
        hand.addCard(aceOfSpades);
        //Test for negative BlackJack
        assertFalse(EvaluateHand.isBlackjack(hand));

        hand.addCard(jackOfDiamonds);
        assertEquals(21, hand.getValue());
        //Test for positive Blackjack
        assertTrue(EvaluateHand.isBlackjack(hand));

        hand.addCard(kingOfHearts);
        assertEquals(21, hand.getValue());
        // Test for negative Blackjack when hand value is 21 but there are more than 2 cards in Hand
        assertFalse(EvaluateHand.isBlackjack(hand));
    }

    /**
     * This test checks that isBust() returns true only if Hand's value is more than 21. And it doesn't depend on number
     * of cards in Hand
     */
    @Test
    public void testIsBust() {
        Hand hand = new Hand();
        hand.addCard(aceOfSpades);
        hand.addCard(jackOfDiamonds);

        //Negative test for bust
        assertFalse(EvaluateHand.isBust(hand));

        //Test to make sure isBust method returns false as long as Hand value is less than equal to 21, even if there
        // are more than 2 cards in hand
        hand.addCard(kingOfHearts);
        assertEquals(21, hand.getValue());
        assertFalse(EvaluateHand.isBust(hand));

        hand.addCard(sixOfClubs);
        //Positive test for Bust
        assertTrue(EvaluateHand.isBust(hand));
    }

}