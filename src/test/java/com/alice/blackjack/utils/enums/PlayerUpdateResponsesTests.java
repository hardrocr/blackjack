package com.alice.blackjack.utils.enums;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * This Test class is used to make sure that correct number and value of PlayerUpdateResponses exists
 */
public class PlayerUpdateResponsesTests {

    /**
     * Test to check number of responses
     */
    @Test
    public void testNumberOfSuits(){
        assertEquals("Testing for number of responses",6, PlayerUpdateResponses.values().length);
    }

    /**
     * Test to check all the responses exists
     */
    @Test
    public void testAllSuitsExist(){
        assertNotNull("Testing GAME_NOT_STARTED exist", PlayerUpdateResponses.valueOf("GAME_NOT_STARTED"));
        assertNotNull("Testing GAME_FINISHED exist", PlayerUpdateResponses.valueOf("GAME_FINISHED"));
        assertNotNull("Testing PLAYER_NOT_IN_GAME exist", PlayerUpdateResponses.valueOf("PLAYER_NOT_IN_GAME"));
        assertNotNull("Testing WAIT_FOR_TURN exist", PlayerUpdateResponses.valueOf("WAIT_FOR_TURN"));
        assertNotNull("Testing INVALID_MOVE exist", PlayerUpdateResponses.valueOf("INVALID_MOVE"));
        assertNotNull("Testing MOVE_RECORDED exist", PlayerUpdateResponses.valueOf("MOVE_RECORDED"));

    }
}
