package com.alice.blackjack.utils.enums;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * This Test class is used to make sure that correct number and value of Suits exists, because Suits enum is used to
 * generate a Deck. There should always be 4 suits - SPADES, CLUBS, HEARTS and DIAMONDS
 */
public class SuitsTests {

    /**
     * Test to check number of suits
     */
    @Test
    public void testNumberOfSuits(){
        assertEquals("Testing for number of Suits",4, Suits.values().length);
    }

    /**
     * Test to check all the suits exists
     */
    @Test
    public void testAllSuitsExist(){
        assertNotNull("Testing SPADES exist", Suits.valueOf("SPADES"));
        assertNotNull("Testing CLUBS exist", Suits.valueOf("CLUBS"));
        assertNotNull("Testing HEARTS exist", Suits.valueOf("HEARTS"));
        assertNotNull("Testing DIAMONDS exist", Suits.valueOf("DIAMONDS"));
    }
}
