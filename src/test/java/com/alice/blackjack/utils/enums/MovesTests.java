package com.alice.blackjack.utils.enums;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * This Test class is used to make sure that correct number of Moves exist
 */
public class MovesTests {

    /**
     * Test to check number of Moves
     */
    @Test
    public void testNumberOfStatuses(){
        assertEquals("Testing for number of Moves",4, Moves.values().length);
    }

    /**
     * Test to check all moves exists and have correct value
     */
    @Test
    public void testAllMovesValue(){
        assertEquals("Testing value of STAND move",     1, Moves.valueOf("STAND").getValue());
        assertEquals("Testing value of HIT move",       2, Moves.valueOf("HIT").getValue());
        assertEquals("Testing value of SPLIT move",     3, Moves.valueOf("SPLIT").getValue());
        assertEquals("Testing value of SURRENDER move", 4, Moves.valueOf("SURRENDER").getValue());
    }
}

