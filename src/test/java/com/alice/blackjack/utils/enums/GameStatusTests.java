package com.alice.blackjack.utils.enums;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * This Test class is used to make sure that correct Game statuses exist
 */
public class GameStatusTests {

    /**
     * Test to check number of game statuses
     */
    @Test
    public void testNumberOfStatuses(){
        assertEquals("Testing for number of Game Statuses",3, GameStatus.values().length);
    }

    /**
     * Test to check all game statuses exists
     */
    @Test
    public void testAllSuitsExist(){
        assertNotNull("Testing OPEN status exists", GameStatus.valueOf("OPEN"));
        assertNotNull("Testing STARTED status exists", GameStatus.valueOf("STARTED"));
        assertNotNull("Testing FINISHED status exists", GameStatus.valueOf("FINISHED"));
    }
}

