package com.alice.blackjack.utils.enums;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * This Test class is used to make sure that correct number Player statuses exist
 */
public class PlayerStatusTests {

    /**
     * Test to check number of player statuses
     */
    @Test
    public void testNumberOfStatuses(){
        assertEquals("Testing for number of Player Statuses",6, PlayerStatus.values().length);
    }

    /**
     * Test to check all player statuses exists
     */
    @Test
    public void testAllStatusesExist(){
        assertNotNull("Testing OPEN status exists", PlayerStatus.valueOf("OPEN"));
        assertNotNull("Testing TURN status exists", PlayerStatus.valueOf("TURN"));
        assertNotNull("Testing WAIT status exists", PlayerStatus.valueOf("WAIT"));
        assertNotNull("Testing WON status exists", PlayerStatus.valueOf("WON"));
        assertNotNull("Testing LOST status exists", PlayerStatus.valueOf("LOST"));
        assertNotNull("Testing DRAW status exists", PlayerStatus.valueOf("DRAW"));
    }
}

