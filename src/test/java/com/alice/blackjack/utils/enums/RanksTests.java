package com.alice.blackjack.utils.enums;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * This Test class is used to make sure that correct number and value of Ranks exists, because Ranks enum is used to
 * generate a Deck. There should always be 13 Ranks - TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN,
 * KING and ACE;
 */
public class RanksTests {

    /**
     * Test to check number of Ranks
     */
    @Test
    public void testNumberOfRanks(){
        assertEquals("Testing for number of Ranks", 13, Ranks.values().length);
    }

    /**
     * Test to check all the Ranks exists and they have correct value
     */
    @Test
    public void testAllRanksValue(){
        assertEquals("Testing value of TWO",    2, Ranks.valueOf("TWO").getValue());
        assertEquals("Testing value of THREE",  3, Ranks.valueOf("THREE").getValue());
        assertEquals("Testing value of FOUR",   4, Ranks.valueOf("FOUR").getValue());
        assertEquals("Testing value of FIVE",   5, Ranks.valueOf("FIVE").getValue());
        assertEquals("Testing value of SIX",    6, Ranks.valueOf("SIX").getValue());
        assertEquals("Testing value of SEVEN",  7, Ranks.valueOf("SEVEN").getValue());
        assertEquals("Testing value of EIGHT",  8, Ranks.valueOf("EIGHT").getValue());
        assertEquals("Testing value of NINE",   9, Ranks.valueOf("NINE").getValue());
        assertEquals("Testing value of TEN",    10, Ranks.valueOf("TEN").getValue());
        assertEquals("Testing value of JACK",   10, Ranks.valueOf("JACK").getValue());
        assertEquals("Testing value of QUEEN",  10, Ranks.valueOf("QUEEN").getValue());
        assertEquals("Testing value of KING",   10, Ranks.valueOf("KING").getValue());
        assertEquals("Testing value of ACE",    11, Ranks.valueOf("ACE").getValue());
    }
}
