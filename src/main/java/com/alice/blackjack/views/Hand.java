package com.alice.blackjack.views;

import com.alice.blackjack.utils.enums.Ranks;

import java.util.LinkedList;

/**
 * Hand class represent a collection of cards in a Player's Hand.
 * <p>
 * This class provides methods related to manipulation of Hand like add card to hand, get value of hand, check if a hand
 * can be split, split a hand.
 * </p>
 *
 * @author      Manpreet Singh
 */
public class Hand {

    private final LinkedList<Card> cards;

    /**
     * Class constructor to create an object of the the class Hand.
     * This constructor create an empty list of cards and set the hand value to 0
     */
    public Hand() {
        cards = new LinkedList<>();
        //value = 0;
    }

    /**
     * This method adds a card to Player's hand and calulate the new value of Hand
     *
     * @param card      Card that needs to be added to the Hand
     *
     * @throws          NullPointerException
     *                  If the card to be added is null
     */
    public void addCard(Card card){
        if(card == null){
            throw new NullPointerException();
        }

        cards.add(new Card(card.getRank(), card.getSuit()));
    }

    /**
     * This method calculates value of hand and returns that value
     *
     * @return      Returns integer value of Hand
     */
    public int getValue(){
        int value = 0;
        // This stores the number of ace's in this hand
        int aceCardCount = 0;

        // Add values of all cards in the hand
        for(Card card : cards) {
            // Check if it is ace card
            if (card.getRank().equals(Ranks.ACE)) {
                // increase the count of ace cards by 1
                aceCardCount++;
            }
            // Add card's value to Hand Value
            value = value + card.getRank().getValue();

        }

        // While hand value is more than 21 and there are Ace cards in Hand, convert Ace value from 11 to 1
        while(aceCardCount > 0 && value > 21){
            // Convert ace value from 11 to 1
            value = value - 10;
            // Reduce card count by 1
            aceCardCount --;
        }

        return value;
    }

    /**
     * This method checks if the Hand can be split into two based on the input parameter sameValueSplitAllowed.
     * Generally same value splits are allowed but behaviour of this method can be changed based on personal preference.
     *
     * @param sameValueSplitAllowed If it is true then cards with different ranks but same value are allowed to split
     *                              E.g. Jack and King will be allowed to split
     *                              If false then cards with same ranks are only allowed to split
     *
     * @return      Returns true if split is possible otherwise returns false
     */
    public boolean canSplit(Boolean sameValueSplitAllowed){
        if(cards.size() == 2){
            if(sameValueSplitAllowed){
                // Check First and Last card have same value
                if(cards.getFirst().getRank().getValue() == cards.getLast().getRank().getValue()){
                    return true;
                } else {
                    return false;
                }
            } else {
                // Check First and Last card have same rank
                if(cards.getFirst().getRank().equals(cards.getLast().getRank())){
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * This method split Hands into two based on the input parameter sameValueSplitAllowed. If split is possible then
     * removes last card from this Hand, add it to new Hand and returns new Hand. If split is not possible then
     * throws IllegalStateException
     *
     * @param sameValueSplitAllowed If it is true then cards with different ranks but same value are allowed to split
     *                              E.g. Jack and King will be allowed to split
     *                              If false then cards with same ranks are only allowed to split
     *
     * @return      Returns new Hand if split is possible otherwise throws exception
     *
     * @throws IllegalStateException
     *         If this Hand is not allowed to split
     */
    public Hand split(Boolean sameValueSplitAllowed){

        // If can't split throw exception
        if(!canSplit(sameValueSplitAllowed)) {
            throw new IllegalStateException("Can't split this Hand");
        }

        Hand newHand = new Hand();
        // Remove last card from this hand and add it to new Hand
        newHand.addCard(this.cards.removeLast());

        return newHand;
    }

    public LinkedList<Card> getCards() {
        return cards;
    }

    @Override
    public String toString() {
        return "Hand{" +
                "cards=" + cards +
                '}';
    }
}
