package com.alice.blackjack.views;

import com.alice.blackjack.utils.enums.Ranks;
import com.alice.blackjack.utils.enums.Suits;

/**
 * Card is a public class which represents a card in the deck.
 * <p>
 * This class provides methods to get rank, suit and to print card.
 * </p>
 *
 * @author      Manpreet Singh
 */

public class Card {

    private Ranks rank;
    private Suits suit;

    /**
     * Class constructor to create an object of the the class Card
     *
     * @param rank      Rank of the card to be created
     * @param suit      Suit of the card to be created
     *
     * @throws          NullPointerException
     *                  If either rank or suit is null
     */

    public Card(Ranks rank, Suits suit) throws NullPointerException{
        if(rank == null || suit == null){
            throw new NullPointerException();
        }
        this.rank = rank;
        this.suit = suit;
    }


    public Ranks getRank() {
        return rank;
    }

    public Suits getSuit() {
        return suit;
    }

    @Override
    public String toString() {
        return "Card{" +
                "rank=" + rank +
                ", suit=" + suit +
                '}';
    }
}
