package com.alice.blackjack.views;

import com.alice.blackjack.utils.enums.PlayerStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represent a Blackjack player or dealer.
 * <p>
 * This class provides methods to manipulate player's Hand.
 * </p>
 *
 * @author      Manpreet Singh
 */
public class Player {

    private final String id;
    private String name;

    private PlayerStatus status;

    //Used a list in case player wants to split
    private final List<Hand> hands = new ArrayList<>();

    public Player(String id, String name) {
        this.id = id;
        this.name = name;

        // Set Player status to OPEN
        status = PlayerStatus.OPEN;
    }

    public Player(String id, String name, PlayerStatus status) {
        this.id = id;
        this.name = name;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Hand> getHands() {
        return hands;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PlayerStatus getStatus() {
        return status;
    }

    public void setStatus(PlayerStatus status) {
        this.status = status;
    }

    public void resetHands(){
        hands.clear();
    }

    @Override
    public String toString() {
        return "Player{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", hands=" + hands +
                '}';
    }
}
