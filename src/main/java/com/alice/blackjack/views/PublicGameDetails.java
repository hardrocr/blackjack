package com.alice.blackjack.views;

import com.alice.blackjack.utils.enums.GameStatus;
import com.alice.blackjack.utils.enums.Moves;
import com.alice.blackjack.utils.enums.PlayerStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This immutable class returns current details of game that can be shared with public.
 * This class have a nested class PublicPlayer. That is just used to attach winnings to player profile because they are
 * not part of player's profile and are handled by Game class
 *
 * @author      Manpreet Singh
 */
public final class PublicGameDetails {
    private final String id;

    private final int maxPlayers;

    private final GameStatus status;

    private final Player dealer;

    private final List<PublicPlayer> players;

    private Player nextTurn;

    private Hand nextHand;

    private Set<Moves> availableMoves;

    public PublicGameDetails (String id, int maxPlayers, GameStatus status, Player dealer, List<Player> players,
                              Map<String, Integer> playerWinnings, Hand nextHand, Player nextTurn,
                              Set<Moves> availableMoves){
        this.id = id;
        this.maxPlayers = maxPlayers;
        this.status = status;
        this.dealer = dealer;
        this.players = new ArrayList<>();

        for(Player player: players){
            this.players.add(new PublicPlayer(player.getId(), player.getName(), player.getStatus(), player.getHands(),
                    playerWinnings.get(player.getId())));
        }

        if(status.equals(GameStatus.STARTED)){
            this.nextHand = nextHand;
            this.nextTurn = nextTurn;
            this.availableMoves = availableMoves;
        }
    }

    public String getId() {
        return id;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public GameStatus getStatus() {
        return status;
    }

    public Player getDealer() {
        return dealer;
    }

    public List<PublicPlayer> getPlayers() {
        return players;
    }

    public Player getNextTurn() {
        return nextTurn;
    }

    public Set<Moves> getAvailableMoves() {
        return availableMoves;
    }

    public Hand getNextHand() {
        return nextHand;
    }

    /**
     * This nested class is just used to add winnings to player object. It is nested because except PublicGameDetails no
     * class should be able to add winnings to player.
     */
    public class PublicPlayer extends Player{

        private final int winnings;

        public PublicPlayer(String id, String name, PlayerStatus status, List<Hand> hands, int winnings) {
            super(id, name, status);
            this.getHands().addAll(hands);
            this.winnings = winnings;
        }

        public int getWinnings() {
            return winnings;
        }

        @Override
        public String toString() {
            return "Player{" +
                    "id='" + this.getId() + '\'' +
                    ", name='" + this.getName() + '\'' +
                    ", hands=" + this.getHands() + '\'' +
                    ", winnings=" + winnings +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "PublicGameDetails{" +
                "id='" + id + '\'' +
                ", maxPlayers=" + maxPlayers +
                ", status=" + status +
                ", dealer=" + dealer +
                ", players=" + players +
                ", nextTurn=" + nextTurn +
                ", nextHand=" + nextHand +
                ", availableMoves=" + availableMoves +
                '}';
    }
}
