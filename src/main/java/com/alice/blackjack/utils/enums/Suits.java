package com.alice.blackjack.utils.enums;

/**
 * Suits is a public enum that provides list of Suits that are available in the deck
 * <p>
 * This enum have four values SPADES, CLUBS, HEARTS and DIAMONDS
 * </p>
 *
 * @author      Manpreet Singh
 */
public enum Suits {
    SPADES, CLUBS, HEARTS, DIAMONDS
}
