package com.alice.blackjack.utils.enums;

/**
 * Ranks is a public enum that provides list of hand values that are available in the deck
 * <p>
 * This enum have 13 values TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE.
 * According to blackjack rules :
 * All the Ranks from TWO to TEN are valued at their face value. JACK, QUEEN and KING are valued 10. ACE is valued at 11
 * unless sum of hands increases 21, in that case it is valued at 1
 *
 * </p>
 *
 * @author      Manpreet Singh
 */
public enum Ranks {
    TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6), SEVEN(7), EIGHT(8), NINE(9), TEN(10), JACK(10), QUEEN(10), KING(10), ACE(11);

    private final int value;
    private Ranks(int value){
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
