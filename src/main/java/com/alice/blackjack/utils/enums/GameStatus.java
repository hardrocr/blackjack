package com.alice.blackjack.utils.enums;

/**
 * GameStatus is a public enum that provides list of statuses for a game
 * <p>
 * This enum have 3 statuses OPEN, STARTED, FINISHED:
 *
 * OPEN         -   Indicates that the game is open and players can join to place initial bets.
 *
 * STARTED      -   Indicates that the game is in progresses and newly joined players can't make initial bets.
 *
 * FINISHED     -   Indicates that dealer finished playing all the players.
 * </p>
 *
 * @author      Manpreet Singh
 */
public enum GameStatus {
    OPEN, STARTED, FINISHED
}
