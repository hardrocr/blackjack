package com.alice.blackjack.utils.enums;

/**
 * Moves is a public enum that provides list of moves that are available in the blackjack game
 * <p>
 * This enum have 5 moves STAND, HIT, DOUBLE_DOWN, SPLIT and SURRENDER:
 *
 * Stand        –   If the player is happy with the total they’ve been dealt they can stand, taking no further action
 *                  and passing to the next player. The player can take this action after any of the other player
 *                  actions as long as their hand total is not more than 21. The hand signal to Stand is waving a flat
 *                  hand over the cards.
 *
 * Hit          –   If the player wishes to take another card they signal to the dealer to by scratching the felt beside
 *                  their hand or pointing to their hand. A single card is then played face up onto their hand. If the
 *                  hand total is less than 21 the player can choose to Hit again or Stand. If the total is 21 the hand
 *                  automatically stands. If the total is over 21 the hand is bust, the player’s bet is taken by the
 *                  house and the turn to act passes to the next player.
 *
 * Split        –   If the player’s first two cards are of matching rank they can choose to place an additional bet
 *                  equal to their original bet and split the cards into two hands. Where the player chooses to do this
 *                  the cards are separated and an additional card is dealt to complete each hand. If either hand
 *                  receives a second card of matching rank the player may be offered the option to split again, though
 *                  this depends on the rules in the casino. Generally the player is allowed a maximum of 4 hands after
 *                  which no further splits are allowed. The split hands are played one at a time in the order in which
 *                  they were dealt, from the dealer's left to the delaer's right. The player has all the usual options:
 *                  stand, hit or double down.
 *
 * Surrender    –   Most casinos allow a player to surrender, taking back half their bet and giving up their hand.
 *                  Surrender must be the player's first and only action on the hand. In the most usual version, known
 *                  as Late Surrender, it is after the dealer has checked the hole card and does not have a Blackjack.
 *
 * </p>
 *
 * @author      Manpreet Singh
 */

public enum Moves {

    STAND(1), HIT(2), SPLIT(3), SURRENDER(4);

    private final int value;

    Moves(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}

