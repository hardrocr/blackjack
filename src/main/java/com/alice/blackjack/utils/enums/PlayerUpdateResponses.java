package com.alice.blackjack.utils.enums;

public enum  PlayerUpdateResponses {

    GAME_NOT_STARTED, GAME_FINISHED, PLAYER_NOT_IN_GAME, WAIT_FOR_TURN, INVALID_MOVE, MOVE_RECORDED

}
