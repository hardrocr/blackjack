package com.alice.blackjack.utils.enums;

/**
 * PlayerStatus is a public enum that provides list of statuses for a player
 * <p>
 * This enum have 6 statuses OPEN, TURN, WAIT, WON, LOST, DRAW:
 *
 * OPEN                 - This indicates that player has not joined any game
 *
 * TURN                 - This indicates that it is player's turn
 *
 * WAIT                 - This indicates that player needs to wait because it is either dealer's or other players' turn
 *
 * WON                  - This indicates that player won this round against dealer
 *
 * LOST                 - This indicates that player lost this round against dealer
 *
 * DRAW                 - This indicates that this round against dealer ended in a draw
 *
 * </p>
 *
 * @author      Manpreet Singh
 */
public enum PlayerStatus {
    OPEN, TURN, WAIT, WON, LOST, DRAW;
}
