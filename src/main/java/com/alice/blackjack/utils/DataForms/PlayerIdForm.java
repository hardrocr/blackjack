package com.alice.blackjack.utils.DataForms;

/**
 * This classed is used to parse the body of HTTP request (path = "/games/{gameId}/players", method = RequestMethod.POST)
 *
 * @author      Manpreet Singh
 */
public class PlayerIdForm {

    public String id;

    public PlayerIdForm() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
