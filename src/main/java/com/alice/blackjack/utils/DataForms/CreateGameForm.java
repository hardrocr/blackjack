package com.alice.blackjack.utils.DataForms;


/**
 * This classed is used to parse the body of HTTP request (path = "/games", method = RequestMethod.POST)
 *
 * @author      Manpreet Singh
 */
public class CreateGameForm {
    private int maxPlayers;
    private int deckSize;

    public CreateGameForm() {
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public int getDeckSize() {
        return deckSize;
    }

    public void setDeckSize(int deckSize) {
        this.deckSize = deckSize;
    }
}
