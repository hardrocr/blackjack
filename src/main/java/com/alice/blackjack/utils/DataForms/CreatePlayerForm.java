package com.alice.blackjack.utils.DataForms;

/**
 * This classed is used to parse the body of HTTP request (path = "/players", method = RequestMethod.POST)
 *
 * @author      Manpreet Singh
 */
public class CreatePlayerForm {

    private String name;

    public CreatePlayerForm() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
