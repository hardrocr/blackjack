package com.alice.blackjack.utils.DataForms;

import com.alice.blackjack.utils.enums.Moves;


/**
 * This classed is used to parse the body of HTTP request (path = "/games/{gameId}/players", method = RequestMethod.POST)
 *
 * @author      Manpreet Singh
 */

public class PlayerMoveForm {
    Moves move;

    public PlayerMoveForm() {
    }

    public Moves getMove() {
        return move;
    }

    public void setMove(Moves move) {
        this.move = move;
    }
}
