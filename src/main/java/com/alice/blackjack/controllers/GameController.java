package com.alice.blackjack.controllers;

import com.alice.blackjack.models.Game;
import com.alice.blackjack.models.GameService;
import com.alice.blackjack.models.PlayerService;
import com.alice.blackjack.utils.DataForms.CreateGameForm;
import com.alice.blackjack.utils.DataForms.CreatePlayerForm;
import com.alice.blackjack.utils.DataForms.PlayerIdForm;
import com.alice.blackjack.utils.DataForms.PlayerMoveForm;
import com.alice.blackjack.utils.enums.PlayerStatus;
import com.alice.blackjack.utils.enums.PlayerUpdateResponses;
import com.alice.blackjack.views.Player;
import com.alice.blackjack.views.PublicGameDetails;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This is the rest controller that provides all the routes for HTTP requests to play Blackjack game
 *
 * @author      Manpreet Singh
 */
@RestController
@RequestMapping("/blackjack")
@Api(value = "blackjack", description = "REST APIs to play Blackjack")
public class GameController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GameController.class);

    @Autowired
    private GameService gameService;

    @Autowired
    private PlayerService playerService;

    @RequestMapping(value = "/players", method = RequestMethod.GET)
    @ApiOperation(value = "Get all players", notes = "Returns a list of all the players")
    @ApiResponses(@ApiResponse(code = 200, message = "OK"))
    public Collection<Player> getAllPlayers(){
        return playerService.getAll();
    }

    @RequestMapping(value = "/players", method = RequestMethod.POST)
    @ApiOperation(value = "Adds a player", notes = "Creates a new player with given name and returns player's info")
    @ApiResponses(@ApiResponse(code = 200, message = "OK"))
    public Player createPlayer(@RequestBody @ApiParam("Name to create a new player") CreatePlayerForm form) {
        Player player = playerService.createPlayer(form.getName());

        LOGGER.info("Player (id = " + player.getId() + ") : player created");

        return player;
    }

    @RequestMapping(value = "/players/{playerId}", method = RequestMethod.GET)
    @ApiOperation(value = "Gets a player", notes = "Returns a player's info based on playerId")
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 400, message = "Player with given ID can't be found")})
    public Player getPlayer(@PathVariable("playerId") @ApiParam("ID of the player who's info needs to be returned") String playerId){
        Player player = playerService.getPlayer(playerId);
        if(player == null){
            throw new IllegalArgumentException("Invalid playerId");
        }
        return player;
    }

    @RequestMapping(value = "/players/{playerId}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Deletes a player", notes = "Removes a player based on playerId and returns deleted player's info")
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 400, message = "Player with given ID can't be found")})
    public Player deletePlayer(@PathVariable("playerId") @ApiParam("ID of the player that needs to be deleted") String playerId){
        Player player = playerService.removePlayer(playerId);
        if(player == null){
            throw new IllegalArgumentException("Invalid playerId");
        }
        LOGGER.info("player (id = " + player.getId() + ") : player deleted");
        return player;
    }

    @RequestMapping(value = "/games", method = RequestMethod.GET)
    @ApiOperation(value = "Get all games", notes = "Returns a list of all the games")
    @ApiResponses(@ApiResponse(code = 200, message = "OK"))
    public Collection<PublicGameDetails> getAllGames(){
        return gameService.getAll().stream().map(Game::getDetails).collect(Collectors.toList());
    }

    @RequestMapping(value = "/games", method = RequestMethod.POST)
    @ApiOperation(value = "Adds a game", notes = "Creates a new game with given number of maximum players that are allowed to play at one time and initializes the game with a deck of given size. Number of cards used in the game are 52 * deckSize")
    @ApiResponses(@ApiResponse(code = 200, message = "OK"))
    public PublicGameDetails createGame(@RequestBody @ApiParam("maxPlayers and deckSize to create a new game") CreateGameForm form) {
        Game newGame = gameService.createGame(form.getMaxPlayers(), form.getDeckSize());

        LOGGER.info("game (id = " + newGame.getId() + ") : game created");

        return newGame.getDetails();
    }

    @RequestMapping(value = "/games/{gameId}", method = RequestMethod.GET)
    @ApiOperation(value = "Gets a game", notes = "Returns a game's info based on gameId")
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 400, message = "Game with given ID can't be found")})
    public PublicGameDetails getGame(@PathVariable("gameId") @ApiParam("ID of the game for which info needs to be returned") String gameId){
        Game game = gameService.getGame(gameId);
        if(game == null){
            throw new IllegalArgumentException("Invalid gameId");
        }
        return game.getDetails();
    }

    @RequestMapping(value = "/games/{gameId}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Deletes a game", notes = "Removes a game based on gameId and returns deleted game's info")
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 400, message = "Game with given ID can't be found")})
    public PublicGameDetails deleteGame(@PathVariable("gameId") @ApiParam("ID of the game that needs to be deleted") String gameId){
        Game game = gameService.removeGame(gameId);
        if(game == null){
            throw new IllegalArgumentException("Invalid gameId");
        }
        LOGGER.info("game (id = " + game.getId() + ") : game deleted");
        return game.getDetails();
    }

    @RequestMapping(value = "/games/{gameId}/players", method = RequestMethod.GET)
    @ApiOperation(value = "Get all the players in a game", notes = "Returns a list of all the players that are playing in a given game")
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 400, message = "Game with given ID can't be found")})
    public List<PublicGameDetails.PublicPlayer> getGamePlayers(@PathVariable("gameId") @ApiParam("ID of the game for which players need to be returned") String gameId){
        Game game = gameService.getGame(gameId);
        if(game == null){
            throw new IllegalArgumentException("Invalid gameId");
        }
        return game.getDetails().getPlayers();
    }

    @RequestMapping(value = "/games/{gameId}/players", method = RequestMethod.POST)
    @ApiOperation(value = "Adds a player to a game", notes = "Returns: \n1. WAIT - If player is added to the game, this indicates player needs to wait till game starts. " +
            "\n2. OPEN - if player can't be added to the game, this indicates that player is open to join other games. This could be because of: " +
            "\n\t\ta) The game that player wants to join has already stared." +
            "\n\t\tb) Maximum number of allowed players have already joined the game so, can't join till someone else leaves this game." +
            "\n\t\tc) If player have already joined some other game, then they will not be allowed to join this game")
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 400, message = "Game or Player with given ID can't be found")})
    public PlayerStatus addGamePlayers(@PathVariable("gameId") @ApiParam("ID of the game to which players need to be added")String gameId, @RequestBody @ApiParam("ID of the player that needs to be added to the game") PlayerIdForm form){
        Game game = gameService.getGame(gameId);
        Player player = playerService.getPlayer(form.getId());
        if(game == null){
            throw new IllegalArgumentException("Invalid gameId");
        }
        if(player == null){
            throw new IllegalArgumentException("Invalid playerId");
        }
        return game.addPlayer(player);
    }

    @RequestMapping(path = "/games/{gameId}/players", method = RequestMethod.DELETE)
    @ApiOperation(value = "Removes given player from the game", notes = "Returns $(dollar) amount that they have earned while playing the game. " +
            "Once a player's winnings drop below $10 they will not be able to make anymore bets so, they will be automatically removed from the game." +
            "\n This api also returns -1 if the given player have not joined given game")
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 400, message = "Game or Player with given ID can't be found")})
    public int removeGamePlayers(@PathVariable("gameId") @ApiParam("ID of the game from which players need to be removed") String gameId, @RequestBody @ApiParam("ID of the player that needs to be removed from the game") PlayerIdForm form){
        Game game = gameService.getGame(gameId);
        Player player = playerService.getPlayer(form.getId());
        if(game == null){
            throw new IllegalArgumentException("Invalid gameId");
        }
        if(player == null){
            throw new IllegalArgumentException("Invalid playerId");
        }
        return game.removePlayer(player);
    }

    @RequestMapping(value = "/games/{gameId}/start", method = RequestMethod.PUT)
    @ApiOperation(value = "Starts a game", notes = "Starts a game that is either OPEN or FINISHED as long as there is at least one player that have joined the game. Returns public details of the game after game has started")
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 400, message = "Game with given ID can't be found")})
    public PublicGameDetails startGame(@PathVariable("gameId") @ApiParam("ID of the game that needs to be started") String gameId){
        Game game = gameService.getGame(gameId);
        if(game == null){
            throw new IllegalArgumentException("Invalid gameId");
        }
        return game.startGame();
    }

    @RequestMapping(value = "/games/{gameId}/players/{playerId}", method = RequestMethod.POST)
    @ApiOperation(value = "Updates the game based of player's requested move", notes = "Returns: " +
            "\n1. GAME_NOT_STARTED: If player requests a move before game even started." +
            "\n2. GAME_FINISHED: If player requests a move after game is finished." +
            "\n3. PLAYER_NOT_IN_GAME: If player requests a move in the game that they haven't joined." +
            "\n4. WAIT_FOR_TURN: If player requests a move before it is their turn to make a move." +
            "\n5. INVALID_MOVE: If player requests a move that is not valid. E.g. Player requests SPLIT when they have a six and an eight cards" +
            "\n6. MOVE_RECORDED: If player requests a valid move on their turn")
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 400, message = "Game or Player with given ID can't be found")})
    public PlayerUpdateResponses updateGamePlayerMove(@PathVariable("gameId") @ApiParam("ID of the game that needs to be updated") String gameId,
                                                      @PathVariable("playerId") @ApiParam("ID of the player that is requesting the move") String playerId, @RequestBody  @ApiParam PlayerMoveForm form){
        Game game = gameService.getGame(gameId);
        Player player = playerService.getPlayer(playerId);
        if(game == null){
            throw new IllegalArgumentException("Invalid gameId");
        }
        if(player == null){
            throw new IllegalArgumentException("Invalid playerId");
        }
        return game.updatePlayer(player, form.getMove());
    }

    @ExceptionHandler
    void handleIllegalArgumentException(IllegalArgumentException e, HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), e.getMessage());
    }
}

