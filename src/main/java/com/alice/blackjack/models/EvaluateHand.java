package com.alice.blackjack.models;

import com.alice.blackjack.views.Hand;

/**
 * This class is used to evaluate cards in a given Hand
 *
 * @author      Manpreet Singh
 */
public class EvaluateHand {

    /**
     * If there are exactly two cards in hand and their sum is 21, then Returns True
     * @param hand  hand that needs to be evaluated
     * @return Either true or false based on given hand
     */
    public static boolean isBlackjack(Hand hand){
        if(hand.getCards().size() == 2 && hand.getValue() == 21){
            return true;
        } else {
            return false;
        }
    }

    /**
     * If sum of all the cards in this hand exceeds 21, then Returns True
     * @param hand  hand that needs to be evaluated
     * @return Either true or false based on given hand
     */
    public static boolean isBust(Hand hand){
        if(hand.getValue() > 21){
            return true;
        } else {
            return false;
        }
    }
}
