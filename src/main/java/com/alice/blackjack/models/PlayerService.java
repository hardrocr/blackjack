package com.alice.blackjack.models;

import com.alice.blackjack.views.Player;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This service class provides all the methods to add, remove and get Players
 *
 * @author      Manpreet Singh
 */
@Service
public class PlayerService {

    private ConcurrentHashMap<String, Player> players = new ConcurrentHashMap<>();

    public Player createPlayer(String name){
        // Generates a random UUID and creates a new player with that id.
        String id = UUID.randomUUID().toString();
        Player player = new Player(id, name);
        players.put(id, player);
        return player;
    }

    public Collection<Player> getAll(){
        return players.values();
    }

    public Player getPlayer(String id){
        return players.get(id);
    }

    public Player removePlayer(String id){
        return players.remove(id);
    }
}
