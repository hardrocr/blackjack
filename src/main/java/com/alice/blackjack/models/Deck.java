package com.alice.blackjack.models;

import com.alice.blackjack.utils.enums.Ranks;
import com.alice.blackjack.utils.enums.Suits;
import com.alice.blackjack.views.Card;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 * Deck is a public class which represents a collection of cards which are in multiples of 52 based on size.
 * <p>
 * This class provides methods to draw and shuffle the deck.
 * </p>
 *
 * @author      Manpreet Singh
 */

public class Deck {

    /**
     * This represent the list of cards in Deck
     */
    private final LinkedList<Card> cards;

    /**
     * Class constructor to create an object of the the class Deck.
     * This constructor add cards based on the size of deck
     *
     * @param size      Represent size of the Deck to be created where, Size = 1 is equivalent to 52 cards in the deck.
     *
     */
    public Deck(int size) {
        cards = new LinkedList<>();
        while (size > 0){
            //add 52 cards which represents all possible combination of ranks and suits
            for(Suits suit : Suits.values()){
                for(Ranks rank : Ranks.values()){
                    cards.add(new Card(rank, suit));
                }
            }

            //Reduce the size by one after one set of 52 cards is added to the list
            size--;
        }
    }

    /**
     * Class constructor to create an object of the the class Deck. This constructor creates immutable copy of the collection
     * of cards provided as parameter.
     *
     * @param cards      Represent set of cards to be added to deck
     *
     */
    public Deck(Collection<Card> cards) {
        this.cards = new LinkedList<>();
        this.cards.addAll(cards);
    }

    /**
     * This method removes first card from the list of cards and returns that card.
     * If there are no cards in the list then throws NoSuchElementException
     *
     * @return      A Card object which is the first element in the list of cards
     *
     * @throws      NoSuchElementException
     *              If there are no more cards left in Deck
     *
     */
    public Card draw() {
        if(cards.isEmpty()){
            throw new NoSuchElementException("No more cards left in Deck");
        } else {
            return cards.removeFirst();
        }
    }

    /**
     * This method shuffles all the cards that are present in the deck
     */
    public void shuffle(){
        Collections.shuffle(cards);
    }
}
