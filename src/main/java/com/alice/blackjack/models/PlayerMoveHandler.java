package com.alice.blackjack.models;

import com.alice.blackjack.utils.enums.Moves;
import com.alice.blackjack.views.Hand;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * This class have one static method that returns all possible available moves for a Hand
 *
 * @author      Manpreet Singh
 */

public class PlayerMoveHandler {

    /**
     * This method calculates all the available moves for a given hand
     * @param hand                  Hand for which moves needs to be calculated
     * @param sameValueSplitAllowed If true then cards with same values are allowed to split
     * @return Set of available Moves
     */
    public static Set<Moves> getAvailableMoves(Hand hand, Boolean sameValueSplitAllowed){
        if (hand == null){
            return null;
        }

        Set<Moves> availableMoves = new LinkedHashSet<>();

        if(hand.getValue() <= 21){
            availableMoves.add(Moves.STAND);
        }

        if(hand.getValue() < 21){
            System.out.print(hand.getValue());
            availableMoves.add(Moves.HIT);
        }

        if(hand.canSplit(sameValueSplitAllowed)){
            availableMoves.add(Moves.SPLIT);
        }

        availableMoves.add(Moves.SURRENDER);

        return availableMoves;
    }
}
