package com.alice.blackjack.models;

import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This service class provides all the methods to add, remove and get Games
 *
 * @author      Manpreet Singh
 */
@Service
public class GameService {
    private ConcurrentHashMap<String, Game> games = new ConcurrentHashMap<>();

    public Game createGame(int maxPlayers, int deckSize){
        // Generates a random UUID and creates a game with that id.
        String id = UUID.randomUUID().toString();
        Game game = new Game(id, maxPlayers, deckSize);
        games.put(id,game);
        return game;
    }

    public Collection<Game> getAll(){
        return games.values();
    }

    public Game getGame(String id){
        return games.get(id);
    }

    public Game removeGame(String id){
        return games.remove(id);
    }
}
