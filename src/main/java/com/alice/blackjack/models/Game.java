package com.alice.blackjack.models;


import com.alice.blackjack.utils.enums.GameStatus;
import com.alice.blackjack.utils.enums.Moves;
import com.alice.blackjack.utils.enums.PlayerStatus;
import com.alice.blackjack.utils.enums.PlayerUpdateResponses;
import com.alice.blackjack.views.Card;
import com.alice.blackjack.views.Hand;
import com.alice.blackjack.views.Player;
import com.alice.blackjack.views.PublicGameDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * This class represent a Blackjack game.
 * <p>
 * This class provides methods to play Blackjack game.
 * </p>
 *
 * @author      Manpreet Singh
 */

public class Game {

    private static final Logger LOGGER = LoggerFactory.getLogger(Game.class);

    // This is the initial money given to players when they join a game
    private int playerStartingAmount = 100;

    // This is the initial bet paid by the player when they start playing
    private int playerBetAmount = 10;

    //If this is true players are allowed to split cards that have same value like JACK and KING
    private boolean sameValueSplitAllowed = true;

    //Game id
    private final String id;

    //Max number players allowed to enter this game
    private int maxPlayers;

    //Number of decks to be used in this game. Number of cards used = 52 * deckSize
    private Integer deckSize;

    //If deck size is not defined this will be used to re create deck
    private Collection<Card> cards;

    //This stores the staus of the game
    private GameStatus status;

    // This object hold all the cards for the game. If all the cards from deck are used game will automatically
    // initialize and shuffle deck.
    private Deck deck;

    // Player object to hold instance of Dealer
    private Player dealer = new Player(UUID.randomUUID().toString(), "Dealer");

    // List of Players that have joined the game
    private final LinkedList<Player> players = new LinkedList<>();

    // This map keep tracks of player winnings if they would like to keep playing this game multiple times.
    // Winnings are returned only if player leaves this game
    private final Map<String, Integer> playerWinnings = new HashMap<>();

    // This object stores instance of next Player
    private Player nextTurn = null;

    // This object stores instance of next Hand of Next Player. Need this because a player can have multiple hands if
    // they decide to split
    private Hand nextHand = null;

    // This stores all available moves for the next player
    private Set<Moves> availableMoves = new LinkedHashSet<>();

    /**
     * Constructor method to create new game
     * @param id
     * @param maxPlayers
     * @param deckSize
     */
    public Game(String id, int maxPlayers, int deckSize) {
        this.id = id;
        this.maxPlayers = maxPlayers;
        this.deckSize = deckSize;

        initializeDeck(deckSize);

        //Set Game status to OPEN
        status = GameStatus.OPEN;
    }

    /**
     * Constructor method to create new game with given cards. Cards will not be shuffled in this case. Every time deck
     * is empty is will be re created with same collection of cards.
     * @param id
     * @param maxPlayers
     * @param
     */
    public Game(String id, int maxPlayers, Collection<Card> cards) {
        this.id = id;
        this.maxPlayers = maxPlayers;
        this.cards = cards;

        initializeDeck(cards);

        //Set Game status to OPEN
        status = GameStatus.OPEN;
    }

    private void initializeDeck(int  deckSize){
        // Add decks to the game
        deck = new Deck(deckSize);

        //Shuffle deck
        deck.shuffle();

        LOGGER.info("game (id = " + id + ") : initialized with " + deckSize + " deck(s)");
    }

    private void initializeDeck(Collection<Card> cards){
        // Add cards to the game
        deck = new Deck(cards);

        LOGGER.info("game (id = " + id + ") : initialized with " + cards.size() + " cards(s)");
    }

    /**
     * This method is called when game is started and returns PublicGameDetails after it finish executing
     *
     * @return
     */
    public PublicGameDetails startGame(){
        //Start game if we have atleast one player and game is open or finished
        if(players.size() > 0 && (status.equals(GameStatus.OPEN) || status.equals(GameStatus.FINISHED))){
            if(status.equals(GameStatus.FINISHED)){
                resetPlayers();
            }
            setGameStatus(GameStatus.STARTED);

            //Deal one card to each player
            for(Player player : players){
                // if player doesn't have enough money to make bet. Remove player from the game
                if(playerWinnings.get(player.getId()) < playerBetAmount){
                    removePlayer(player);
                }

                //Remove bets from each player before dealing cards
                updatePlayerWinnings(player, - playerBetAmount);

                //Create a empty hand for this player
                player.getHands().add(new Hand());

                dealInitialCardsToPlayer(player);
            }

            //Create a empty hand for this player
            dealer.getHands().add(new Hand());

            //Deal one card to Dealer
            dealInitialCardsToPlayer(dealer);

            // Deal one more card to each player
            for(Player player : players){
                dealInitialCardsToPlayer(player);
            }

            //Deal one more card to Dealer
            dealInitialCardsToPlayer(dealer);

            //Perform initial game update to check if player or dealer have Blackjack
            initialGameUpdate();

            if(!status.equals(GameStatus.FINISHED)){
                //Pass control to next player
                passControlToNextHand(dealer, dealer.getHands().get(0));
            }
        }

        PublicGameDetails gameDetails = getDetails();
        LOGGER.debug("game (id = " + id + ") : game details" + gameDetails);
        return gameDetails;
    }

    /**
     * This method update this game's status and logs an entry in Logger
     * @param newStatus
     */
    private void setGameStatus(GameStatus newStatus){
        if(newStatus == null){
            return;
        }

        LOGGER.info("game (id = " + id + ") : game status changed from " + status + " to " + newStatus);

        status = newStatus;
    }

    /**
     * This method is used if game is restarted. it is used to reset all the hands for all players and dealer
     */
    private void resetPlayers(){
        //No need to create new deck on restart game will automatically handle if once deck is finished
        // Clear all the data from last game
        for(Player player : players){
            //Reset their hands
            player.resetHands();
            setPlayerStatus(player, PlayerStatus.WAIT);
        }

        //reset dealer
        dealer.resetHands();
        //Next is dealer's turn to distribute the cards
        setPlayerStatus(dealer, PlayerStatus.TURN);
    }

    /**
     * This method update player's status and logs an entry in Logger
     * @param player
     * @param newStatus
     */
    private void setPlayerStatus(Player player, PlayerStatus newStatus){
        if(player == null || newStatus == null){
            return;
        }
        LOGGER.info("game (id = " + id + ") : player (id = " + player.getId() + ") status changed from " +
                        player.getStatus() + " to " + newStatus);
        player.setStatus(newStatus);
    }

    /**
     * This method is used to deal one card to all of this player's hands
     * @param player
     */
    private void dealInitialCardsToPlayer(Player player) {
        if(player == null){
            return;
        }

        // Loop over all player hands and give one to each hand
        for (Hand hand : player.getHands()) {
            addCardToHand(hand);
        }

        LOGGER.info("game (id = " + id + ") : finished dealing to Player :" + player);
    }

    /**
     * This method is use to draw a card from deck and add it to hand
     * @param hand
     */
    private void addCardToHand(Hand hand){
        if(hand == null){
            return;
        }

        try{
            //Deal one card to this hand
            Card card = deck.draw();
            hand.addCard(card);
            LOGGER.debug("game (id = " + id + ") : last card drawn :"+ card);

        } catch (NoSuchElementException ex){
            LOGGER.warn("game (id = " + id + ") : " + ex.toString(), ex);
            //Re initialize Deck and draw a card
            if(deckSize == null){
                //initialize using cards
                initializeDeck(cards);
            } else {
                //initialize using deck size
                initializeDeck(deckSize);
            }
            addCardToHand(hand);
        }
    }

    /**
     * method to add a player to the game.
     * @param player
     * @return          If OPEN is returned - Player was not added to this game and they are open to join other games
     *                  If WAIT is returned - Player is added to this game and they should wait till game starts
     *                  If null is returned - Player was invalid
     */
    public PlayerStatus addPlayer(Player player){
        if(player == null){
            return null;
        }
        synchronized (this){
            // If game is started
            if(status.equals(GameStatus.STARTED)) {
                return PlayerStatus.OPEN;
            }

            // Check if more players can be added to the game
            if(players.size() < maxPlayers){

                //Check if player is already playing some other or this game
                if(!player.getStatus().equals(PlayerStatus.OPEN)){
                    return PlayerStatus.OPEN;
                }

                // Add player to list of game players
                players.add(player);

                // Set status to wait because game haven't started
                setPlayerStatus(player, PlayerStatus.WAIT);

                // Start each player with player Starting Amount
                playerWinnings.put(player.getId(), playerStartingAmount);

                LOGGER.info("game (id = " + id + ") : player (id = " + player.getId() + ") joined");

                return PlayerStatus.WAIT;

            } else {
                return PlayerStatus.OPEN;
            }
        }
    }

    /**
     * Method to remove a player to the game. This method returns their winnings when the player decide to leave
     * @param player
     * @return
     */
    public int removePlayer(Player player){
        if(player == null){
            return 0;
        }

        // These winnings can be transferred to their Bank Accounts
        player.resetHands();

        players.remove(player);

        // Open play status so that they can join other games
        setPlayerStatus(player, PlayerStatus.OPEN);

        // Set winnings to -1 so that if player doesn't exists. This method returns -1
        int winnings = -1;
        if(playerWinnings.containsKey(player.getId())){
            winnings = playerWinnings.get(player.getId());
            playerWinnings.remove(player.getId());

            LOGGER.info("game (id = " + id + ") : player (id = " + player.getId() + ") left with $" + winnings + " winnings");

        }
        return winnings;
    }

    /**
     * This methods creats an immutable object that is used to store the current details of the game
     * @return
     */
    public PublicGameDetails getDetails(){
        return new PublicGameDetails(id, maxPlayers, status, dealer, players, playerWinnings, nextHand, nextTurn, availableMoves);
    }

    /**
     * This method is called after all the players and dealer have received two cards each
     */
    private void initialGameUpdate(){
        //Check if dealer have blackjack
        if(EvaluateHand.isBlackjack(dealer.getHands().get(0))){
            //Loop over all Players and calculate if won or draw
            for(Player player: players){
                //If player already made their turn
                if(player.getStatus().equals(PlayerStatus.WAIT)){
                    // Loop over all the hands of this player
                    for(Hand hand : player.getHands()){
                        if(EvaluateHand.isBlackjack(hand)){
                            player.setStatus(PlayerStatus.DRAW);

                            //return their winnings
                            updatePlayerWinnings(player, playerBetAmount);
                        } else{
                            player.setStatus(PlayerStatus.LOST);
                        }
                    }
                }
            }

            //Game finished
            setGameStatus(GameStatus.FINISHED);
        } else {
            // Pay all players who have blackjack
            //Loop over all Players and calculate if won or not
            int playersHaveBlackjack = 0;
            for(Player player: players){
                //If player already made their turn
                if(player.getStatus().equals(PlayerStatus.WAIT)){
                    // Loop over all the hands of this player
                    for(Hand hand : player.getHands()){
                        if(EvaluateHand.isBlackjack(hand)){
                            player.setStatus(PlayerStatus.WON);
                            playersHaveBlackjack ++;
                            //return their winnings (intial bet + 3/2 times initial bet)
                            updatePlayerWinnings(player, (playerBetAmount * 5) / 2);
                        }
                    }
                }
            }

            //if all players have blackjack game is finished
            if(players.size() == playersHaveBlackjack){
                //Game finished
                setGameStatus(GameStatus.FINISHED);
            }
        }
        return;
    }

    /**
     * This method updates winning of given player by given value
     * @param player
     * @param value
     */
    private void updatePlayerWinnings(Player player, int value){
        playerWinnings.put(player.getId(), playerWinnings.get(player.getId()) + value);
    }

    /**
     * This method updates game so that dealer can finish it's turn
     */
    private void updateGame(){
        // Dealer is next make a move
        if(nextTurn.equals(dealer)){
            Hand dealerHand = dealer.getHands().get(0);
            if(dealerHand.getValue() < 17){
                // Dealer will get a card
                addCardToHand(dealerHand);

                PublicGameDetails gameDetails = getDetails();
                LOGGER.debug("game (id = " + id + ") : game details" + gameDetails);

                //Update game
                updateGame();
            } else if (EvaluateHand.isBust(dealerHand)){
                // Pay all waiting players
                //Loop over all Players
                for(Player player: players){
                    //If player already made their turn
                    if(player.getStatus().equals(PlayerStatus.WAIT)){
                        // Loop over all the hands of this player
                        for(Hand hand : player.getHands()) {
                            player.setStatus(PlayerStatus.WON);

                            //return their winnings
                            updatePlayerWinnings(player, playerBetAmount * 2);
                        }
                    }
                }
            } else {

                //If there are more players above dealer. Dealer will draw a card to minimize the winnings to give
                if(areWaitingPlayersAboveDealer(dealerHand)){
                    // Dealer will get a card
                    addCardToHand(dealerHand);

                    PublicGameDetails gameDetails = getDetails();
                    LOGGER.debug("game (id = " + id + ") : game details" + gameDetails);

                    // Check if dealer is not bust
                    if(!EvaluateHand.isBust(dealerHand)){

                        //Check if drawing a card improved dealer's situation
                        //If situation improved
                        if(!areWaitingPlayersAboveDealer(dealerHand)){
                            //pass control to next player
                            passControlToNextHand(dealer, dealerHand);
                        }
                    }
                    //Update game
                    updateGame();
                } else {
                    //Dealer will stand, so do nothing
                }

                //Loop over all Players to find if their total is less or more than dealer
                for(Player player: players){
                    //If player already made their turn
                    if(player.getStatus().equals(PlayerStatus.WAIT)){
                        // Loop over all the hands of this player
                        for(Hand hand : player.getHands()) {
                            if(EvaluateHand.isBust(hand) || hand.getValue() < dealerHand.getValue() ){
                                player.setStatus(PlayerStatus.LOST);
                            } else  if(hand.getValue() > dealerHand.getValue()){
                                player.setStatus(PlayerStatus.WON);

                                //return their winnings (twice the initial bet)
                                updatePlayerWinnings(player, playerBetAmount * 2);
                            } else {
                                player.setStatus(PlayerStatus.DRAW);

                                //return their winnings (initial Bet)
                                updatePlayerWinnings(player, playerBetAmount);
                            }
                        }
                    }
                }
            }

            //Game finished
            setGameStatus(GameStatus.FINISHED);
        }

        PublicGameDetails gameDetails = getDetails();
        LOGGER.debug("game (id = " + id + ") : game details" + gameDetails);
    }

    private boolean areWaitingPlayersAboveDealer(Hand dealerHand){
        //Loop over all Players and find number of players that have value below or above dealers hand
        int aboveDealer = 0;
        int belowDealer = 0;
        // Dont worry about the players that have same value as dealer because that will just
        for(Player player: players){
            //If player already made their turn
            if(player.getStatus().equals(PlayerStatus.WAIT)){
                // Loop over all the hands of this player
                for(Hand hand : player.getHands()) {
                    if(EvaluateHand.isBust(hand) || hand.getValue() < dealerHand.getValue() ){
                        belowDealer ++;
                    } else  if(hand.getValue() > dealerHand.getValue()){
                        aboveDealer ++;
                    }
                }
            }
        }

        //If there are more players above dealer. Dealer will draw a card to minimize its winnings
        if(aboveDealer > belowDealer){
            return true;
        } else {
            return false;
        }
    }

    /**
     * This method update the player's  nexthand based on the given Move and then pass the control to the next hand in
     * the game
     * @param player
     * @param move
     * @return
     */
    public PlayerUpdateResponses updatePlayer(Player player, Moves move){

        //Check if game is in correct state to accept the request
        if(status.equals(GameStatus.OPEN)){
            return PlayerUpdateResponses.GAME_NOT_STARTED;
        }

        if(status.equals(GameStatus.FINISHED)){
            return PlayerUpdateResponses.GAME_FINISHED;
        }

        // check if player have joined the game
        if(!playerWinnings.containsKey(player.getId())){
            return PlayerUpdateResponses.PLAYER_NOT_IN_GAME;
        }

        // Check if it is player's turn
        if(!nextTurn.equals(player)){
            return PlayerUpdateResponses.WAIT_FOR_TURN;
        }

        if(!availableMoves.contains(move)){
            return PlayerUpdateResponses.INVALID_MOVE;
        }

        switch(move){
            case STAND:
                passControlToNextHand(player, nextHand);
                break;
            case HIT:
                addCardToHand(nextHand);
                if(nextHand.getValue() >= 21){
                    passControlToNextHand(player, nextHand);
                }
                break;
            case SPLIT:
                // if player doesn't have enough money to make bet. Remove player from the game
                if(playerWinnings.get(player.getId()) < playerBetAmount){
                    removePlayer(player);
                }

                //If they split remove winnings from account
                updatePlayerWinnings(player, -playerBetAmount);

                Hand splitHand = nextHand.split(sameValueSplitAllowed);
                //Add card to next and this split hand
                addCardToHand(nextHand);
                addCardToHand(splitHand);

                //Add split hand to list of player's hand
                player.getHands().add(splitHand);
                break;
            case SURRENDER:
                setPlayerStatus(player, PlayerStatus.LOST);
                passControlToNextHand(player, nextHand);
                break;
        }

        PublicGameDetails gameDetails = getDetails();
        LOGGER.debug("game (id = " + id + ") : game details" + gameDetails);
        return PlayerUpdateResponses.MOVE_RECORDED;
    }

    /**
     * This method is called after a player or dealer have made their move
     * @param fromPlayer
     * @param fromHand
     */
    private void passControlToNextHand(Player fromPlayer, Hand fromHand) {
        //If this is dealer
        if(fromPlayer.equals(dealer)) {
            //Select first waiting player and pass control to them
            int i;
            for(i = 0 ; i < players.size(); i ++){
                Player potentialNextPlayer = players.get(i);
                if(potentialNextPlayer.getStatus().equals(PlayerStatus.WAIT)){
                    nextTurn = potentialNextPlayer;
                    nextHand = potentialNextPlayer.getHands().get(0);
                    availableMoves = PlayerMoveHandler.getAvailableMoves(nextHand, sameValueSplitAllowed);
                    break;
                }
            }
            //if no players are waiting game is Finished
            if(i == players.size()){
                setGameStatus(GameStatus.FINISHED);
            } else {
                setPlayerStatus(fromPlayer, PlayerStatus.WAIT);
                setPlayerStatus(nextTurn, PlayerStatus.TURN);
            }
        } else{
            //Find next waiting player and pass control to them
            //find index of from hand
            int fromHandIndex = fromPlayer.getHands().indexOf(fromHand);
            if(fromHandIndex == -1){
                //Hand doesn't belong to from player
                return;
            }
            //Check if there is atleast one more hand after current hand
            if((fromHandIndex + 1) < fromPlayer.getHands().size()){
                // from player is our next player
                nextHand = fromPlayer.getHands().get(fromHandIndex + 1);
                availableMoves = PlayerMoveHandler.getAvailableMoves(nextHand, sameValueSplitAllowed);
            } else {
                //Find next waiting player in line
                int i = players.indexOf(fromPlayer);
                for(; i < players.size(); i ++){
                    Player potentialNextPlayer = players.get(i);
                    if(potentialNextPlayer.getStatus().equals(PlayerStatus.WAIT)){
                        nextTurn = potentialNextPlayer;
                        nextHand = potentialNextPlayer.getHands().get(0);
                        availableMoves = PlayerMoveHandler.getAvailableMoves(nextHand, sameValueSplitAllowed);
                        break;
                    }
                }
                //if no players are waiting pass control to dealer
                if(i == players.size()){
                    setPlayerStatus(fromPlayer, PlayerStatus.WAIT);
                    setPlayerStatus(dealer, PlayerStatus.TURN);
                    nextTurn = dealer;
                    nextHand = dealer.getHands().get(0);
                } else {
                    setPlayerStatus(fromPlayer, PlayerStatus.WAIT);
                    setPlayerStatus(nextTurn, PlayerStatus.TURN);
                }
            }
        }

        updateGame();
    }

    public String getId() {
        return id;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public Integer getDeckSize() {
        return deckSize;
    }

    public int getPlayerStartingAmount() {
        return playerStartingAmount;
    }

    public int getPlayerBetAmount() {
        return playerBetAmount;
    }

    public boolean isSameValueSplitAllowed() {
        return sameValueSplitAllowed;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setPlayerStartingAmount(int playerStartingAmount) {
        this.playerStartingAmount = playerStartingAmount;
    }

    public void setPlayerBetAmount(int playerBetAmount) {
        this.playerBetAmount = playerBetAmount;
    }

    public void setSameValueSplitAllowed(boolean sameValueSplitAllowed) {
        this.sameValueSplitAllowed = sameValueSplitAllowed;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }
}
