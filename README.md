# BlackJack REST API #

This maven project is developed using Spring Boot (spring-boot-starter-web). It provides RESTful APIs for the Blackjack game. Junit and Mackito are used to test the application and Swagger is used to provide UI to test and document the apis. 

### Set up ###
Download the project from bitbucket. 

For command line make sure maven and java 8 is installed. Run the project using command:

`mvn spring-boot:run`

Or import the project in your favourite IDE IntelliJ or Eclipse.

Once project is running, open following URL in your browser to interact with API using a UI provided by Swagger

http://localhost:8080/swagger-ui.html




Follow these steps to play the game
# BlackJack REST API #

This maven project is developed using Spring Boot (spring-boot-starter-web). It provides RESTful APIs for the Blackjack game. Junit and Mackito are used to test the application and Swagger is used to provide UI to test and document the apis. 

### Set up ###
Download the project from bitbucket. 

For command line make sure maven and java 8 is installed. Run the project using command:

`mvn spring-boot:run`

Or import the project in your favourite IDE IntelliJ or Eclipse.

Once project is running, open following URL in your browser to interact with API using a UI provided by Swagger

`http://localhost:8080/swagger-ui.html`




Follow these steps to play the game

1. Create a new player using 'POST /blackjack/players' by providing player's name
2. Create a new game using 'POST /blackjack/games' by providing maximum number of players allowed and deck size for the game
3. Call 'GET /blackjack/players' and 'GET /blackjack/games' to get ids of newly created player and game.
4. Call 'POST /blackjack/games/{gameId}/players' to add player to the game. You can create more players and them to the same game using this api.
5. Call 'PUT /blackjack/games/{gameId}/start' to start the game of blackjack. It returns details of game after after game has started.
6. Finally, call 'POST /blackjack/games/{gameId}/players/{playerId}' to make a move out of list of available moves given in game details.
7. Call 'GET /blackjack/games/{gameId}' to get details of game after each move



Some of the key info returned in Game's details

* ID of game given by id
* Status of game : OPEN, STARTED or FINISHED
* Dealer's Hand
* List of players and their Hands
* Next player in line to make a move is given by nextTurn.
* List of available moves for the next player